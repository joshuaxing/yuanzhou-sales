/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-10-21 16:45:21
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
import { Toast } from "antd-mobile";
import http from "@/fetch";
import headicon from "./head-icon@2x.png";
@connect(
  (state) => state.reducer,
  (dispatch) => bindActionCreators(actions, dispatch)
)
class User extends React.Component {
  state = {
    result: {
      nickname: "",
      amount: "--",
      cumulative_amount: "--",
      logourl: "",
    },
    rootList: [],
  };

  componentDidMount() {
    this.gotData();
    this.handleCheck();
  }
  gotData() {
    const user = this.props.user;
    const param = {
      saleid: user.userId,
    };
    http("/sale/user/info", {
      data: param,
    }).then((result) => {
      if (result.code === 0) {
        const data = result.data;
        this.setState({
          result: data,
        });
      } else {
        Toast.info(result.msg);
      }
    });
  }
  handleCheck() {
    const user = this.props.user;
    const param = {
      saleid: user.userId,
    };
    http(
      "/sale/user/activity",
      {
        data: param,
      },
      { isload: false }
    ).then((result) => {
      if (result.code === 0) {
        const data = result.data;
        this.setState({
          rootList: data.activitytype,
        });
      } else {
        Toast.info(result.msg);
      }
    });
  }
  goWithdraw() {
    this.props.history.push("/withdraw");
  }
  handleSet() {
    this.props.history.push("/userSet");
  }
  handleNav(value) {
    const { rootList } = this.state;
    const isroot = rootList.some((rootvalue) => {
      return rootvalue === value;
    });
    if (!isroot && value < 6) {
      Toast.fail("您目前没有该权限");
      return;
    }
    switch (value) {
      case 1:
        // 公众号
        this.props.history.push("/sharegzh");
        break;
      case 2:
        // 分销注册会员
        this.props.history.push("/sharewx");
        break;
      case 3:
        // 分销金卡
        this.props.history.push("/sharegold");
        break;
      case 4:
        // 订房分销
        this.props.history.push("/hotel");
        break;
      case 5:
        // 分销活动

        break;
      case 6:
        // 我的佣金
        this.props.history.push("/brokerage");
        break;
      case 7:
        // 排行榜
        this.props.history.push("/rankirngList");
        break;
      case 8:
        // 常见问题
        this.props.history.push("/commonIssue");
        break;
      default:
    }
  }
  handlePirce(value) {
    return typeof value === "number" ? value.toFixed(2) : "0.00";
  }
  render() {
    const { result } = this.state;
    return (
      <div className="user-page page">
        <Helmet>
          <title>我的</title>
        </Helmet>
        <div className="user-header">
          <div className="header-content">
            <img
              src={result.logourl ? result.logourl : headicon}
              className="header-image"
            />
            <div className="header-other">
              <span className="header-name">{result.nickname}</span>
            </div>
          </div>
          <div className="header-set">
            <div className="set-btn" onClick={this.handleSet.bind(this)}>
              设置
            </div>
            {/* <div className="set-icon"></div> */}
          </div>
          <div className="money-card">
            <div className="card-item">
              <div className="card-num">
                {this.handlePirce(result.cumulative_amount)}
              </div>
              <div className="card-des">累计佣金</div>
            </div>
            <div className="card-item">
              <div className="card-num">{this.handlePirce(result.amount)}</div>
              <div className="card-des">预估佣金</div>
            </div>
            <div className="card-item" onClick={this.goWithdraw.bind(this)}>
              <div className="card-qianbao">
                <span className="qianbao-icon"></span>
                <span className="qianbao-text">提现</span>
              </div>
            </div>
          </div>
        </div>
        <div className="user-content">
          <ul className="userlist-ul">
            <li
              className="userlist-item"
              onClick={this.handleNav.bind(this, 4)}
            >
              <span className="userlist-item-icon jd-icon"></span>
              <span className="userlist-item-text">分享酒店预订</span>
            </li>
            <li
              className="userlist-item"
              onClick={this.handleNav.bind(this, 5)}
            >
              <span className="userlist-item-icon activity-icon"></span>
              <span className="userlist-item-text">分享活动</span>
            </li>
            <li
              className="userlist-item"
              onClick={this.handleNav.bind(this, 2)}
            >
              <span className="userlist-item-icon hy-icon"></span>
              <span className="userlist-item-text">分享注册会员</span>
            </li>
            <li
              className="userlist-item"
              onClick={this.handleNav.bind(this, 3)}
            >
              <span className="userlist-item-icon fxcard-icon"></span>
              <span className="userlist-item-text">分享金卡</span>
            </li>
            <li
              className="userlist-item"
              onClick={this.handleNav.bind(this, 1)}
            >
              <span className="userlist-item-icon user-icon"></span>
              <span className="userlist-item-text">关注公众号</span>
            </li>
            <li
              className="userlist-item"
              onClick={this.handleNav.bind(this, 6)}
            >
              <span className="userlist-item-icon yongjin-icon"></span>
              <span className="userlist-item-text">我的佣金</span>
            </li>
            <li
              className="userlist-item"
              onClick={this.handleNav.bind(this, 7)}
            >
              <span className="userlist-item-icon paihangban-icon"></span>
              <span className="userlist-item-text">排行榜</span>
            </li>
            <li
              className="userlist-item"
              onClick={this.handleNav.bind(this, 8)}
            >
              <span className="userlist-item-icon que-icon"></span>
              <span className="userlist-item-text">常见问题</span>
            </li>
            <li className="userlist-item-abox">
              <a href="tel:021-39558888" className="userlist-item">
                <span className="userlist-item-icon relation-icon"></span>
                <span className="userlist-item-text">联系总部</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
export default User;
