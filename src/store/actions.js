/*
 * @Author: your name
 * @Date: 2020-03-03 11:19:50
 * @LastEditTime: 2020-12-30 14:39:48
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\store\actions.js
 */

//登录
export const Login = (data, callback) => async (dispatch, getState) => {
  dispatch({
    type: "Login",
    payload: data,
  });
  callback && callback();
};

//客房登录
export const Login2 = (data, callback) => async (dispatch, getState) => {
  dispatch({
    type: "Login2",
    payload: data,
  });
  callback && callback();
};

// 点击页面
export const ClickPage = (data, callback) => async (dispatch, getState) => {
  dispatch({
    type: "clickPage",
    payload: data
  });
  callback && callback();
};
