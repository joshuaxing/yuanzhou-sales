/*
 * @Author: your name
 * @Date: 2020-03-09 10:25:28
 * @LastEditTime: 2020-12-30 14:39:22
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\store\test.js
 */
const initialState = {
  page: ''
};

export default function clickPage(state = initialState, action) {
  const { type, payload } = action;
  switch (type) {
    case 'clickPage':
      // console.log('clickPage')
      return {
        ...state,
        page: payload
      }
    default:
      return state;
  }
}