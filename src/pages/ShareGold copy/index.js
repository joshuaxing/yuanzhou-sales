/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-06-10 14:24:56
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
import http from "@/fetch";
import { Toast } from "antd-mobile";
// import html2canvas from "html2canvas";
import Loading from "@/components/Loading";
import weixinlogo from "./logo.png";
import goldtitle from "./gold-title.png";
import bg from "./bg.jpg";
import BackBar from "@/components/BackBar/index";
import { RESOURCEURL, QLOGO } from "@/constants/index";

// console.log(RESOURCEURL)
// console.log(QLOGO)

@connect(
  (state) => state.reducer,
  (dispatch) => bindActionCreators(actions, dispatch)
)
class ShareGZH extends React.Component {
  state = {
    showimage: false,
    codeurl: "",
    headurl: "",
    nickname: "",
    isload: true,
    money: 50,
  };
  componentDidMount() {
    this.gotData();
  }
  gotData() {
    const user = this.props.user;
    const param = {
      saleid: user.userId,
      saletype: 4,
    };

    http("/sale/user/wxacode", {
      data: param,
    }).then((result) => {
      if (result.code === 0) {
        const data = result.data;
        const code_url = data.code_url;
        const logourl = data.logourl;
        const nickname = data.nickname;
        const money = data.amount ? data.amount : "";
        const originurl =
          window.location.protocol + "//" + window.location.host + "/image";
        const originlogourl =
          window.location.protocol + "//" + window.location.host + "/logo";
        let codeurl = code_url;

        // const testurl = 'https://yuanzhou-resource-test.oss-cn-shanghai.aliyuncs.com';

        if (code_url) {
          codeurl = code_url.replace(RESOURCEURL, originurl);
        }
        let headurl = logourl;
        if (logourl) {
          headurl = logourl.replace(QLOGO, originlogourl);
        }

        // console.log(codeurl)
        // console.log(headurl)

        this.setState({
          codeurl: codeurl,
          headurl: headurl,
          money: money,
        });

        document.getElementById("nickname").innerHTML = nickname
          ? nickname
          : "";
      } else {
        Toast.info(result.msg);
      }
    });
  }
  loadCompletedLogo() {
    // console.log('头像加载完成')
  }
  loadCompleted() {
    window.pageYOffset = 0;
    document.documentElement.scrollTop = 0;
    document.body.scrollTop = 0;
    setTimeout(() => {
      this.initCanvas();
    }, 1500);
  }
  initCanvas() {
    const that = this;
    const targetDom = document.getElementById("capture");
    html2canvas(targetDom, {
      allowTaint: false,
      useCORS: true,
      height: targetDom.clientHeight,
      width: targetDom.clientWidth,
    }).then((canvas) => {
      const url = canvas.toDataURL();
      const image = new Image();
      image.src = url;
      image.style.width = `${targetDom.clientWidth}px`;
      image.style.height = `${targetDom.clientHeight}px`;
      image.onload = function () {
        that.setState(
          {
            showimage: true,
            isload: false,
          },
          () => {
            image.style.transform = `scale(0.9)`;
          }
        );
      };
      document.getElementById("image-box").appendChild(image);
    });
  }
  render() {
    const { showimage, headurl, codeurl, isload, money } = this.state;
    return (
      <div className="share-page page">
        <Helmet>
          <title>金卡分享</title>
        </Helmet>
        <BackBar></BackBar>
        {!showimage ? (
          <div className="share-content-box">
            <div className="share-content" id="capture">
              <img src={bg} className="share-content-bg" />
              <div className="share-content-info">
                <div className="logo-box">
                  <img src={weixinlogo} className="logo" />
                </div>
                <div className="font-icon-box">
                  <img src={goldtitle} className="font-icon" />
                </div>
                <div className="yz-tips">赠送300元大礼包</div>
                {/* <div className="share-gold">
                  <div className="gold-name">悦廷会·金卡会籍</div>
                  <div className="gold-money">
                    预计佣金：￥<span id="money"></span>
                  </div>
                </div> */}
                <div className="code-box">
                  <img
                    src={codeurl}
                    className="code"
                    onLoad={this.loadCompleted.bind(this)}
                  />
                </div>
                <div className="gold-code-tips">长按立即购买金卡会员</div>
                <div className="gold-user-box">
                  <img
                    src={headurl}
                    className="gold-user-image"
                    onLoad={this.loadCompletedLogo.bind(this)}
                  />
                  <div className="gold-user-text">
                    来自<span id="nickname"></span>的分享
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : null}

        <div className="create-logo">
          <div id="image-box"></div>
          <div className={`create-logo-tips`} id="image-text">
            <div className={`tips-p ${!isload ? "active" : ""}`}>
              长按可保存图片，分享至微信群或朋友圈。
            </div>
            <div className={`tips-p ${!isload ? "active" : ""}`}>
              好友购买成功后您将获得
              <span className="tips-p-span">{money}元</span>现金奖励噢~
            </div>
          </div>
        </div>

        {isload && (
          <div className="load-mask">
            <Loading></Loading>
          </div>
        )}
      </div>
    );
  }
}
export default ShareGZH;
