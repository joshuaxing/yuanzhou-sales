/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-12-30 14:13:26
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
import BackBar from "@/components/BackBar/index";
import http from "@/fetch/ajax";
import { Toast } from "antd-mobile";
@connect(
  (state) => state.service,
  (dispatch) => bindActionCreators(actions, dispatch)
)
class Detail extends React.Component {
  state = {
    result: {},
    remark: '',
    leases: '',
    detailid: ''
  };

  componentDidMount() {
    const id = this.props.location.state.id;
    this.setState({
      detailid: id
    },() => {
      this.gotData()
    })
  }

  gotData() {
    const param = {
      id: this.state.detailid,
    };
    http("/room/service/staff/order/detail", {
      data: param,
    }).then((result) => {
      if (result.code === 0) {
        const data = result.data;
        const leasesArr = data.leases ? data.leases : [];
        let leases = ''
        leasesArr.map((item) => {
          leases += `${item.goodsname}、`
        })
        leases = leases ? leases.substring(0, leases.length - 1) : ''
        this.setState({
          result: data,
          leases: leases
        });
      } else {
        Toast.info(result.msg);
      }
    });
  }

  handleSure() {
    const param = {
      id: this.state.detailid
    };
    http("/room/service/staff/order/confirm", {
      data: param,
    }).then((result) => {
      Toast.info(result.msg);
      if (result.code === 0) {
        this.gotData()
      }
    });
  }

  handleComplete() {
    const param = {
      id: this.state.detailid,
      service_remark: this.state.remark
    };
    http("/room/service/staff/order/finish", {
      data: param,
    }).then((result) => {
      Toast.info(result.msg);
      if (result.code === 0) {
        this.gotData()
      }
    });
  }

  handleSeriveTime() {
    const { service_time, service_end_time } = this.state.result
    if (service_end_time) {
      const start = this.handleDateStr(service_time);
      const end = this.handleDateStr(service_end_time);
      return `${end.year}-${end.month}-${end.day} ${start.hour}~${end.hour}点`
    } else {
      return service_time
    }
  }

  handleDateStr(value) {
    const arr = value.split(' ');
    const datearr = arr[0].split('-');
    const timearr = arr[1].split(':')
    return {
      year: datearr[0],
      month: this.fillZero(datearr[1]),
      day: this.fillZero(datearr[2]),
      hour: this.fillZero(timearr[0]),
      minute: this.fillZero(timearr[1]),
      second: this.fillZero(timearr[2])
    }
  }

  fillZero(value) {
    let num = parseInt(value);
    return num < 10 ? "0" + num : num;
  }

  handleNameInput = (e) => {
    const value = e.target.value;
    this.setState({
      remark: value
    });
  };

  render() {
    const { result, remark, leases } = this.state
    return (
      <div className="orderdetail-page page">
        <Helmet>
          <title>服务单详情</title>
        </Helmet>
        <BackBar></BackBar>
        <div className="orderdetail-page-bg">
          <div className="orderdetail-info">
            <div className="order-des">
              <div className="name">{result.categoryname}</div>
              {
                result.roomno && (<div className="room">房间号: {result.roomno}</div>)
              }
            </div>
            <div className="order-status">
              {result.status === 1 ? '待确认' : result.status === 2 ? '待服务' : result.status === 3 ? '服务完成' : result.status === 4 ? '已取消' : ''}
            </div>
          </div>
          <div className="orderdetail-content-box">
            <div className="orderdetail-content">
              
              {
                result.status === 1 && (<div className="content-part content-partone">
                  <div className="order-operation order-operation-big">
                    <div className="sure-btn operation-btn operation-btn-big" onClick={this.handleSure.bind(this)}>确认服务</div>
                  </div>
                </div>)
              }

              {/* <div className="content-part content-partone">
                <div className="order-operation order-operation-big">
                  <div className="sure-btn operation-btn operation-btn-big" onClick={this.handleSure.bind(this)}>确认服务</div>
                  <div className="sure-btn operation-btn operation-btn-big" onClick={this.handleComplete.bind(this)}>服务完成</div>
                </div>
              </div> */}

              <div className="content-part content-partthree model-shodow">
                <div className="partthree-row-title">服务订单信息</div>
                <div className="partthree-row">
                  <div className="row-name">服务单号：</div>
                  <div className="row-content">{result.orderno}</div>
                </div>
                <div className="partthree-row">
                  <div className="row-name">服务酒店：</div>
                  <div className="row-content">{result.hotelname}</div>
                </div>
                {
                  result.roomno && ( <div className="partthree-row">
                    <div className="row-name">服务房间：</div>
                    <div className="row-content">{result.roomno}</div>
                  </div>)
                }
                <div className="partthree-row">
                  <div className="row-name">服务名称：</div>
                  <div className="row-content">{result.categoryname}</div>
                </div>
                <div className="partthree-row">
                  <div className="row-name">预约时间：</div>
                  <div className="row-content">{this.handleSeriveTime()}</div>
                </div>
                <div className="partthree-row">
                  <div className="row-name">客户姓名：</div>
                  <div className="row-content">{result.guest_name}</div>
                </div>
                {
                  result.pickup_address && (<div className="partthree-row">
                    <div className="row-name">接车地址：</div>
                    <div className="row-content">{result.pickup_address}</div>
                  </div>)
                }
                {
                  leases && (<div className="partthree-row">
                    <div className="row-name">租赁物品：</div>
                    <div className="row-content">{leases}</div>
                  </div>)
                }
                <div className="partthree-row">
                  <div className="row-name">联系电话：</div>
                  <div className="row-content">{result.guest_phone}</div>
                </div>
                <div className="partthree-row">
                  <div className="row-name">下单时间：</div>
                  <div className="row-content">{result.ord_createDate}</div>
                </div>
                <div className="partthree-row">
                  <div className="row-name">备注：</div>
                  <div className="row-content">{result.guest_remark}</div>
                </div>
              </div>

              {
                (result.status !== 4 && result.status >= 2) && (<div className="content-part content-partthree model-shodow">
                  <div className="partthree-row-title">酒店接单信息</div>
                  <div className="partthree-row">
                    <div className="row-name">确认时间：</div>
                    <div className="row-content">{result.confirm_time}</div>
                  </div>
                  {
                    result.status >= 3 && (<div className="partthree-row">
                      <div className="row-name">完成时间：</div>
                      <div className="row-content">{result.service_finish_time}</div>
                    </div>)
                  }
                  <div className="partthree-row">
                    <div className="row-name">服务人员：</div>
                    <div className="row-content">{result.service_username}</div>
                  </div>
                  <div className="partthree-row">
                    <div className="row-name">联系电话：</div>
                    <div className="row-content">{result.service_phone}</div>
                  </div>
                  {
                    result.service_remark && (<div className="partthree-row">
                      <div className="row-name">服务备注：</div>
                      <div className="row-content">{result.service_remark}</div>
                    </div>)
                  }
                </div>)
              }

              {
                result.status === 2 && (<div className="content-part content-partthree model-shodow">
                  <div className="partthree-row-title">服务备注</div>
                  <div className="textarea-mark">
                    <textarea className="textarea" placeholder="请输入服务备注"
                      onChange={this.handleNameInput}
                      value={remark}
                    ></textarea>
                  </div>
                </div>)
              }

              {
                result.status === 2 && (<div className="content-part content-partone">
                  <div className="order-operation order-operation-big">
                    <div className="sure-btn operation-btn operation-btn-big" onClick={this.handleComplete.bind(this)}>服务完成</div>
                  </div>
                </div>)
              }

            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Detail;
