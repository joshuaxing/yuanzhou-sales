/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-05-20 17:57:59
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
import BScroll from "better-scroll";
import { Toast } from "antd-mobile";
import http from "@/fetch";
import NotData from "@/components/NotData";
@connect(
  state => state.reducer,
  dispatch => bindActionCreators(actions, dispatch)
)
class Record extends React.Component {
  state = {
    result: [],
    notdata: false
  };

  componentDidMount() {
    this.gotData();
  }

  gotData() {
    const user = this.props.user;
    const param = {
      saleid: user.userId
    };
    http("/sale/user/commission/record", {
      data: param
    }).then(result => {
      if (result.code === 0) {
        const data = result.data;
        const resultdata = data.filter((item) => (
          item.record_list.length > 0
        ))
        const notdata = resultdata.length > 0 ? false : true;
        this.setState({
          result: resultdata,
          notdata: notdata
        });
      } else {
        Toast.info(result.msg);
      }
    });
  }
  initScroll() {
    // let scroll = new BScroll(this.refs.recordwrapper, {});
  }
  computedHeight(vheight) {
    const pageheight = document.body.clientHeight;
    return pageheight - vheight - 50 + "px";
  }
  handlePirce (value) {
    return typeof value === 'number' ? value.toFixed(2) : '0.00'
  }
  render() {
    const { result, notdata } = this.state;
    return (
      <div className="record-page page">
        <Helmet>
          <title>佣金记录</title>
        </Helmet>
        <div className="record-content">
          {result.map(item => (
            <div className="record-content-li" key={item.month}>
              <div className="record-date">
                <span className="date-value">{item.month}</span>
              </div>
              <div className="record-ul-wrapper" ref="recordwrapper">
                <ul className="record-ul">
                  {item.record_list &&
                    item.record_list.map((list, index)=> (
                      <li className="record-li" key={String(index)}>
                        <div className="record-li-left">
                          <div className="record-name">
                            {list.saletype === 4
                              ? "提现"
                              : list.saletype === 3
                              ? "金卡分享到账"
                              : ""}
                          </div>
                          <div className="record-time">{list.date}</div>
                        </div>
                        <div className="record-li-right">
                          <span
                            className={`price ${
                              list.saletype !== 4 ? "red-price" : ""
                            }`}
                          >
                            {list.saletype === 4 ? "-" : "+"}¥{this.handlePirce(list.amount)}
                          </span>
                        </div>
                      </li>
                    ))}
                </ul>
              </div>
            </div>
          ))}
          {
            notdata &&  <NotData></NotData>
          }
         
          {/* <div className="record-content-li">
            <div className="record-date">
              <span className="date-value">2019年7月</span>
              <span className="date-icon"></span>
            </div>
            <div className="record-ul-wrapper" ref="recordwrapper">
              <ul className="record-ul">
                <li className="record-li">
                  <div className="record-li-left">
                    <div className="record-name">佣金提现</div>
                    <div className="record-time">2019-09-29 12:56:20</div>
                  </div>
                  <div className="record-li-right">
                    <span className="price">-¥5.00</span>
                  </div>
                </li>
                <li className="record-li">
                  <div className="record-li-left">
                    <div className="record-name">佣金收益</div>
                    <div className="record-detail">
                      【金卡销售】NO.801759420
                    </div>
                    <div className="record-time">2019-09-29 12:56:20</div>
                  </div>
                  <div className="record-li-right">
                    <span className="price red-price">+¥5.00</span>
                  </div>
                </li>
              </ul>
            </div>
          </div> */}

        </div>
      </div>
    );
  }
}
export default Record;
