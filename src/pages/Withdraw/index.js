/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-06-18 10:56:00
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
import { Toast } from "antd-mobile";
import http from "@/fetch";
import BackBar from "@/components/BackBar/index";
@connect(
  state => state.reducer,
  dispatch => bindActionCreators(actions, dispatch)
)
class Withdraw extends React.Component {

  state = {
    amount: '0.00'
  }
  componentDidMount() {
    this.gotData()
  }

  componentWillUnmount () {}

  goCommonIssue () {
    this.props.history.push('/commonIssue')
  }
  gotData() {
    const user = this.props.user;
    const param = {
      saleid: user.userId
    };
    http("/sale/user/cash", {
      data: param
    }).then(result => {
      if (result.code === 0) {
        const data = result.data;
        const amount = typeof data.amount === 'number' ? data.amount.toFixed(2) : '0.00'
        this.setState({
          amount: amount
        });
      } else {
        Toast.info(result.msg);
      }
    });
  }

  submitMoney () {
    Toast.info("抱歉，此功能目前暂未开放");
  }
  render() {

    return (
      <div className="withdraw-page page">
        <Helmet>
          <title>佣金提现</title>
        </Helmet>
        <BackBar></BackBar>
        <div className="money-icon"></div>
        <div className="money-tips">可提现佣金</div>
        <div className="money-value">¥<strong>{this.state.amount}</strong></div>
        <div className="money-submit-btn" onClick={this.submitMoney.bind(this)}>
          提 现
        </div>
        <div className="money-types">
          <div className="money-type">
            <span className="type-icon weixin-icon"></span>
            <div className="type-text">微信零钱</div>
          </div>
          {/* <div className="type-line"></div>
          <div className="money-type">
            <span className="type-icon yth-icon"></span>
            <div className="type-text">悦廷会・账户</div>
          </div> */}
        </div>
        <div className="common-issue" onClick={this.goCommonIssue.bind(this)}>常见问题</div>
      </div>
    );
  }
}
export default Withdraw;
