/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-11-11 16:34:13
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
import http from "@/fetch";
import { Toast } from "antd-mobile";
import CanvasItem from "@/components/Canvas";
import Loading from "@/components/Loading";
import logo from "./logo.png";
import title from "./gold-title.png";
import bg from "./bg.jpg";
import aaa from "./aaa.png";
import bbb from "./bbb.jpg";
import BackBar from "@/components/BackBar/index";
import { RESOURCEURL, QLOGO } from "@/constants/index";

// console.log(RESOURCEURL)
// console.log(QLOGO)

@connect(
  (state) => state.reducer,
  (dispatch) => bindActionCreators(actions, dispatch)
)
class ShareGOLD extends React.Component {
  state = {
    isload: true,
    money: '',
    imgpath: '',
    canvaswidth: 750, // 画布宽度
    canvasheight: 1334, // 画布高度
    views: [
      {
        type: "image",
        path: bg,
        top: 0,
        left: 0,
        width: 750,
        height: 1334,
      },
      {
        type: "image",
        path: logo,
        top: 80,
        left: 40,
        width: 220,
        height: 35
      },
      {
        type: "image",
        path: title,
        top: 180,
        left: 105,
        width: 540,
        height: 112
      },
      {
        type: "text",
        content: "赠送300元大礼包",
        fontSize: "28px Arial",
        color: "#8A6543",
        textAlign: "center",
        top: 352,
        left: 0
      },
      {
        type: "rect",
        background: '#fff',
        top: 500,
        left: 225,
        width: 300,
        height: 300,
        bordeRadius: 16
      },
      {
        type: "text",
        content: "长按立即购买金卡会员",
        fontSize: "14px Arial",
        color: "#8A6543",
        textAlign: "center",
        top: 840,
        left: 0
      }
    ]
  };
  componentDidMount() {
    this.gotData();
  }
  gotData() {
    const user = this.props.user;
    const param = {
      saleid: user.userId,
      saletype: 4
    };

    http("/sale/user/wxacode", {
      data: param,
    }).then((result) => {
      if (result.code === 0) {
        const data = result.data;
        const code_url = data.code_url;
        const logourl = data.logourl;
        const nickname = data.nickname;
        const money = data.amount ? data.amount : "";
        const originurl =
          window.location.protocol + "//" + window.location.host + "/image";
        const originlogourl =
          window.location.protocol + "//" + window.location.host + "/logo";
        let codeurl = code_url;

        // const testurl = 'https://yuanzhou-resource-test.oss-cn-shanghai.aliyuncs.com';

        if (code_url) {
          codeurl = code_url.replace(RESOURCEURL, originurl);
        }
        let headurl = logourl;
        if (logourl) {
          headurl = logourl.replace(QLOGO, originlogourl);
        }

        console.log(codeurl)
        console.log(headurl)


        const views = [
          {
            type: "image",
            path: bg,
            top: 0,
            left: 0,
            width: 750,
            height: 1334,
          },
          {
            type: "image",
            path: logo,
            top: 80,
            left: 40,
            width: 220,
            height: 35
          },
          {
            type: "image",
            path: title,
            top: 180,
            left: 105,
            width: 540,
            height: 112
          },
          {
            type: "text",
            content: "赠送300元大礼包",
            fontSize: "28px Arial",
            color: "#8A6543",
            textAlign: "center",
            top: 352,
            left: 0
          },
          {
            type: "rect",
            background: '#fff',
            top: 500,
            left: 225,
            width: 300,
            height: 300,
            bordeRadius: 16
          },
          {
            type: "image",
            path: codeurl,
            top: 525,
            left: 250,
            width: 250,
            height: 250
          },
          {
            type: "text",
            content: "长按立即购买金卡会员",
            fontSize: "14px Arial",
            color: "#8A6543",
            textAlign: "center",
            top: 840,
            left: 0
          },
          {
            type: "image",
            path: headurl,
            top: 1000,
            left: 341,
            width: 68,
            height: 68
          },
          {
            type: "text",
            content: `来自${nickname}的分享`,
            fontSize: "14px Arial",
            color: "#8A6543",
            textAlign: "center",
            top: 1088,
            left: 0
          },
        ]
        // console.log(views)
        this.setState({
          money: money,
          views: views
        });
      } else {
        Toast.info(result.msg);
      }
    });
  }
  loadCompleted() {
    this.setState({
      isload: false
    });
  }
  callback (params) {
    const { tempFilePath, errMsg } = params;
    if (errMsg === "canvasdrawer:ok") {
      this.setState({
        imgpath: tempFilePath
      });
    }
  }

  
  render() {
    const {  isload, money, imgpath, views, canvaswidth, canvasheight } = this.state;
    return (
      <div className="share-page page">
        <Helmet>
          <title>金卡分享</title>
        </Helmet>
        <BackBar></BackBar>
        {
          imgpath ? (
            <div className="create-logo">
            <img src={imgpath} alt="加载中..." className={`canvas-image ${!isload ? "active" : ""}`} onLoad={this.loadCompleted.bind(this)}/>
            <div className={`create-logo-tips`} id="image-text">
              <div className={`tips-p ${!isload ? "active" : ""}`}>
                长按可保存图片，分享至微信群或朋友圈。
              </div>
              <div className={`tips-p ${!isload ? "active" : ""}`}>
                好友购买成功后您将获得
                <span className="tips-p-span">{money}元</span>现金奖励噢~
              </div>
            </div>
          </div>
          ) : (
            money && <CanvasItem views={views} canvaswidth={canvaswidth} canvasheight = {canvasheight} callback={this.callback.bind(this)}></CanvasItem>
          )
        }
        
        {isload && (
          <div className="load-mask">
            <Loading></Loading>
          </div>
        )}
      </div>
    );
  }
}
export default ShareGOLD;
