/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-06-10 14:25:16
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
import http from "@/fetch";
import { Toast } from "antd-mobile";
// import html2canvas from "html2canvas";
import weixinlogo from "./weixin-logo.png";
import Loading from "@/components/Loading";
import BackBar from "@/components/BackBar/index";
import { RESOURCEURL, QLOGO} from '@/constants/index';

@connect(
  state => state.reducer,
  dispatch => bindActionCreators(actions, dispatch)
)
class ShareGZH extends React.Component {
  state = {
    showimage: false,
    codeurl: "",
    headurl: "",
    nickname: "",
    isload: true
  };
  componentDidMount() {
    this.gotData();
  }
  gotData() {
    const user = this.props.user;
    const param = {
      saleid: user.userId,
      saletype: 3
    };
    http("/sale/user/wxacode", {
      data: param
    }).then(result => {
      if (result.code === 0) {
        const data = result.data;
        const code_url = data.code_url;
        const logourl = data.logourl;
        const nickname = data.nickname;
        const originurl = window.location.protocol+"//"+window.location.host + '/image';
        const originlogourl = window.location.protocol+"//"+window.location.host + '/logo';
        let codeurl = code_url;
        
        // const testurl = 'https://yuanzhou-resource-test.oss-cn-shanghai.aliyuncs.com';
        // const prourl = 'https://yuanzhou-resource.oss-cn-shanghai.aliyuncs.com';
        
        if (code_url) {
          codeurl = code_url.replace(RESOURCEURL, originurl)
        }
        let headurl = logourl;
        if (logourl) {
          headurl = logourl.replace(QLOGO, originlogourl)
        }
        // console.log(codeurl)
        // console.log(headurl)
        this.setState({
          codeurl: codeurl,
          headurl: headurl
        });
        document.getElementById('nickname').innerHTML = nickname ?  nickname : '';
      } else {
        Toast.info(result.msg);
      }
    });
  }
  loadCompletedLogo () {
    // console.log('头像加载完成')
  }
  loadCompleted() {
    window.pageYOffset = 0;
    document.documentElement.scrollTop = 0;
    document.body.scrollTop = 0;
    setTimeout(() => {
      this.initCanvas();
    }, 1000)
  }
  initCanvas() {
    const that = this;
    const targetDom = document.getElementById("capture");
    html2canvas(targetDom, {
      allowTaint: false,
      useCORS: true,
      height: targetDom.clientHeight,
      width: targetDom.clientWidth
    }).then(canvas => {
      const url = canvas.toDataURL();
      const image = new Image();
      image.src = url;
      image.style.width = `${targetDom.clientWidth}px`;
      image.style.height = `${targetDom.clientHeight}px`;
      image.onload = function() {
        that.setState(
          {
            showimage: true,
            isload: false
          },
          () => {
            image.style.transform = `scale(0.9)`;
          }
        );
      };
      // Node.prototype.prependChild = function (newNode){
      //   this.insertBefore(newNode,this.firstChild);
      // }
      const firstChild = document.getElementById("image-text")
      document.getElementById("image-box").insertBefore(image, firstChild);
    });
  }
  
  render() {
    const { showimage, codeurl, headurl, nickname, isload } = this.state;
    return (
      <div className="sharegzh-page page">
        <Helmet>
          <title>注册会员分享</title>
        </Helmet>
        <BackBar></BackBar>
        {!showimage ? (
          <div className="gzhshare-content-wrapper">
            <div className="gzhshare-content" id="capture">
              <div className="gzhlogo-box">
                <img src={weixinlogo} className="gzhlogo" />
              </div>
              <div className="gzh-text">远洲酒店小程序</div>
              <div className="gzhcode-box" >
                <img src={codeurl} className="gzhcode"  onLoad={this.loadCompleted.bind(this)}/>
              </div>
              <div className="gzhcode-text">长按注册新用户</div>
              <div className="user-box">
                <img
                  src={headurl}
                  className="user-image"
                  onLoad={this.loadCompletedLogo.bind(this)}
                />
                <div className="user-text">来自<span id="nickname"></span>的分享</div>
              </div>
            </div>
          </div>
        ) : null}

        {isload && (
          <div className="load-mask">
            <Loading></Loading>
          </div>
        )}

        <div className="create-gzhlogo" id="image-box">
          <div className={`gzhcode-tips ${!isload ? 'active': ''}`} id="image-text">
            长按可保存图片，分享至微信群或朋友圈
          </div>
        </div>
      </div>
    );
  }
}
export default ShareGZH;
