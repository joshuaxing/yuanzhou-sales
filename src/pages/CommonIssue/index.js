/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-05-26 16:43:06
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
import BackBar from "@/components/BackBar/index";

@connect(
  state => state,
  dispatch => bindActionCreators(actions, dispatch)
)
class CommonIssue extends React.Component {

  state = {
    
  }

  componentDidMount() {
   
  }

  handleDetail (value) {
    switch (value) {
      case 1:
        this.props.history.push('/issueOne')
        break;
        default:
    }
  }
  goBack () {
    console.log(666)
  }
  render() {
    return (
      <div className="issue-page page">
        <Helmet>
          <title>常见问题</title>
        </Helmet>
        <BackBar></BackBar>
        <ul className="issue-content">
          <li className="issue-li" onClick={this.handleDetail.bind(this, 1)}>
            <div className="issue">1. 微信分享注意事项<span className="important-color">(重要)</span></div>
            <span className="issue-icon"></span>
          </li>
          <li className="issue-li">
            <div className="issue">2. 佣金结算说明</div>
            <span className="issue-icon"></span>
          </li>
          <li className="issue-li">
            <div className="issue">3. 分享活动链接操作方式</div>
            <span className="issue-icon"></span>
          </li>
          <li className="issue-li">
            <div className="issue">4. 分享海报操作方式</div>
            <span className="issue-icon"></span>
          </li>
          <li className="issue-li">
            <div className="issue">5. 如何申请员工账号？</div>
            <span className="issue-icon"></span>
          </li>
          <li className="issue-li">
            <div className="issue">6. 佣金提现准备要素</div>
            <span className="issue-icon"></span>
          </li>
          <li className="issue-li">
            <div className="issue">7. 如何申请分享产品、活动？</div>
            <span className="issue-icon"></span>
          </li>
        </ul>
      </div>
      
    );
  }
}
export default CommonIssue;
