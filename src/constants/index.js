/*
 * @Author: your name
 * @Date: 2020-05-25 17:13:32
 * @LastEditTime: 2020-05-25 19:05:28
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-sales\src\config\index.js
 */

/* eslint-disable */

console.log(nodeenv)
console.log(wxappid)
console.log(wxurl)
console.log(resourceurl)

// 微信appid
export const WXAPPID = wxappid;

// 微信登录获取code跳转地址
export const WXURL = wxurl;

// 生成海报路径
export const RESOURCEURL = resourceurl;

// 微信头像
export const QLOGO = qlogo;

// 静态图片
export const STATICURL = `${staticurl}/common`;


// 客房服务
export const SERVERICON = `https://pic.snhotelsgroup.com`;