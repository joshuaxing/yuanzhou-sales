/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-06-11 14:44:56
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
@connect(
  (state) => state.reducer,
  (dispatch) => bindActionCreators(actions, dispatch)
)
class CanvasPage extends React.Component {
  static defaultProps = {
    canvaswidth: 160, // 画布宽度
    canvasheight: 160, // 画布高度
    x0: 80,
    y0: 80,
    r: 72,
    lineWidth: 16,
    strokeStyle: "rgba(248, 248, 248, 1)",
    LinearGradientColor1: "#3EECED",
    LinearGradientColor2: "#499BE6",
    Percentage: 0.3
  };
  state = {
    per: 1
  };

  constructor(props) {
    super(props);
    // this.canvas = React.createRef();
  }

  componentDidMount() {
    // this.canvas.current
    //console.log(this.canvas)

    const per = this.gotper();
    this.setState({
      per: per
    }, () => {
      this.initCanvas();
    })
  }

  componentWillUnmount() {}

  initCanvas() {
    const {
      x0, //原点坐标
      y0,
      r, //半径
      lineWidth, //画笔宽度
      strokeStyle, //画笔颜色
      LinearGradientColor1, //起始渐变颜色
      LinearGradientColor2, //结束渐变颜色
      Percentage //进度百分比
    } = this.props;

    const per = this.state.per;
    
    let ele = document.getElementById("time_graph_canvas");
    let circle = ele.getContext("2d");

    //创建背景圆
    circle.lineWidth = lineWidth*per;
    circle.strokeStyle = strokeStyle;
    circle.lineCap = "round";
    circle.beginPath(); //开始一个新的路径
    circle.arc(x0*per, y0*per, r*per, 0, 2 * Math.PI, false); //用于绘制圆弧context.arc(x坐标，y坐标，半径，起始角度，终止角度，顺时针/逆时针)
    circle.stroke(); //对当前路径进行描边

    //创建渐变圆环

    let g = circle.createLinearGradient(
      x0*per,
      0*per,
      (x0 + r * Math.cos(Percentage * (Math.PI * 2)))*per,
      (y0 + r * Math.sin(this.props.Percentage * (Math.PI * 2))*per)
    ); //创建渐变对象  渐变开始点和渐变结束点
    g.addColorStop(0, LinearGradientColor1); //添加颜色点
    g.addColorStop(1, LinearGradientColor2);
    circle.lineWidth = lineWidth*per; //设置线条宽度
    circle.lineCap = "round";
    circle.strokeStyle = g;
    circle.beginPath(); //开始一个新的路径
    circle.arc(
      x0*per,
      y0*per,
      r*per,
      (-Math.PI / 2)*per,
      (-Math.PI / 2 - Percentage * (Math.PI * 2))*per,
      true
    );
    circle.stroke(); //对当前路径进行描边
  }

  createImg() {
    let img = new Image();
    img.setAttribute("crossOrigin", "Anonymous");
    img.src = url;
    return img;
  }
  convertCanvasToImage() {
    let image = new Image();
    image.src = canvas.toDataURL("image/png");
    return image;
  }
  dpr() {
    if (window.devicePixelRatio && window.devicePixelRatio > 1) {
      return window.devicePixelRatio;
    }
    return 1;
  }

  gotper () {
    const systemWidth =  document.documentElement.clientWidth || document.body.clientWidth;
    const scale = (systemWidth/750).toFixed(2);
    return scale
  }

  render() {
    const { width, height, canvaswidth, canvasheight } = this.props;
    const { per } = this.state
    return (
      <div className="canvas-page page">
        <Helmet>
          <title>Canvas</title>
        </Helmet>
        <div style={{ width: width, height: height, padding: 10 }}>
          <canvas
            id="time_graph_canvas"
            width={canvaswidth*per}
            height={canvasheight*per}
            ref={this.canvas}
          ></canvas>
        </div>
      </div>
    );
  }
}
export default CanvasPage;
