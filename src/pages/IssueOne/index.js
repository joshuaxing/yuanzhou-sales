/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-05-26 16:51:19
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
import BackBar from "@/components/BackBar/index";

@connect(
  state => state,
  dispatch => bindActionCreators(actions, dispatch)
)
class IssueOne extends React.Component {

  state = {
   
  }

  componentDidMount() {
    
  }
  render() {
    return (
      <div className="issueDetail-page page">
        <Helmet>
          <title>微信分享注意事项</title>
        </Helmet>
        <BackBar></BackBar>
        <div className="issueDetail-content">
          <div className="issueDetail-header">微信公众号平台服务协议</div>
          <p>【首部及导言】</p>
          <p>欢迎你使用微信公众平台！</p>
          <p>为了使用微信公众平台服务（以下简称“本服务”），你应当阅读并遵守《微信公众平台服务协议》 （以下简称“本协议”）以及与此相关的专项规则等。</p>
          <p>本服务属于腾讯提供的众多服务之一，你在具体使用过程中，将会不可避免地使用与腾讯微信软件相关的服务，基于这些因素，你还需阅读并遵守《腾讯服务协议》 以及《腾讯微信软件许可及服务协议》 。</p>
          <p>请你务必审慎阅读、充分理解各条款内容，特别是免除或限制责任的相应条款，以及开通或使用某项服务的单独协议或特别条款，并选择接受或不接受。限制、免责条款可能以加粗形式提示你注意。</p>
          <p>除非你已阅读并接受本协议所有条款，否则你无权使用微信公众平台服务。你对本服务的登录、查看、发布信息等行为即视为你已阅读并同意本协议的约束。你有违反本协议的任何行为时，腾讯有权依照违反情况，随时单方限制、中止或终止向你提供本服务，并有权追究你的相关责任。</p>
          <p>基于本服务，你可以申请使用微信公众平台提供的特殊功能服务，若需开通和使用特殊功能服务，请仔细阅读相关特殊功能服务条款，你一旦接受或使用任何特殊功能服务，即视为你已经阅读并同意接受本协议及该特殊功能服务条款，受到本协议及该特殊功能服务条款的约束。</p>
        </div>
      </div>
    );
  }
}
export default IssueOne;
