/*
 * @Author: your name
 * @Date: 2020-03-03 11:52:46
 * @LastEditTime: 2020-04-07 09:38:30
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\api\index.js
 */
import http from '@/utils/http'
let baseURL = '/api';
/**
 * @param  {Objece} params 参数
 * @return {Promise}       包含抓取任务的Promise
 */
export default (api, params, other) => {
  let url = baseURL + api;
  let defaultOption = {
    isload: true
  }
  let option = {};
  if (other) {
    option = Object.assign({}, defaultOption, other);
  } else {
    option = Object.assign({}, defaultOption);
  }
  return http.post(url, params, option)
}