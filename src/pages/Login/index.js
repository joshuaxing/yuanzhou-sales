/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-12-30 15:28:02
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import { Toast, Modal, Button } from "antd-mobile";
import http from "@/fetch";
import * as actions from "@/store/actions";
import loginbanner from "./loginbanner.jpg";
import { WXAPPID, WXURL } from "@/constants/index";
// console.log(WXAPPID)
// 微信获取code重定向
//pro -> wx84fa311acd389ef7
//test -> wxd2ab617c21194709
// const appid = 'wx84fa311acd389ef7'
//pro -> http://sale.snhotelsgroup.com
//test-> http://h5test.snhotelsgroup.com
// const redirect_uri = 'http://sale.snhotelsgroup.com/login'
const weixinurl = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=${WXAPPID}&redirect_uri=${encodeURIComponent(
  WXURL
)}&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect`;

const alert = Modal.alert;

@connect(
  (state) => state.reducer,
  (dispatch) => bindActionCreators(actions, dispatch)
)
class Login extends React.Component {
  codeTimer = null;
  state = {
    passaction: true,
    phone: "",
    code: "",
    password: "",
    lasttime: false,
    ischeck: false,
    disabledCode: true,
    codetext: "获取验证码",
    timenum: 60,
  };

  componentDidMount() {
    //console.log(resourceurl)
    const urlcode = this.getQueryString("code");
    if (!urlcode) {
      window.location.href = weixinurl;
    }
  }

  handleBlur() {
    window.scrollTo(0, 0);
  }
  handlePhoneInput = (e) => {
    const value = e.target.value;
    if (!/^1\d{10}$/.test(value)) {
      this.setState({
        ischeck: false,
        phone: value,
        disabledCode: true,
      });
    } else {
      //手机号正确
      if (!this.state.lasttime) {
        this.setState({
          disabledCode: false,
          ischeck: true,
          phone: value,
        });
      } else {
        //倒计时中
        this.setState({
          disabledCode: true,
          ischeck: true,
          phone: value,
        });
      }
    }
  };

  submitCode = () => {
    const { phone, disabledCode } = this.state;
    if (disabledCode) {
      return;
    }
    //获取验证码
    http("/member/verify", {
      data: phone,
    }).then((result) => {
      if (result.code === 0) {
        //倒计时
        this.countTime();
      }
      Toast.info(result.msg);
    });
  };

  handleCodeInput = (e) => {
    const value = e.target.value;
    this.setState({
      code: value,
    });
  };

  handlePasswordInput = (e) => {
    const value = e.target.value;
    this.setState({
      password: value,
    });
  };

  countTime() {
    //倒计时
    this.setState(
      {
        codetext: this.state.timenum + "s",
        disabledCode: true,
        lasttime: true,
      },
      () => {
        this.codeTimer && clearInterval(this.codeTimer);
        this.codeTimer = setInterval(() => {
          this.setState((state, props) => ({
            timenum: state.timenum - 1,
            codetext: state.timenum + "s",
          }));
          if (this.state.timenum <= 0) {
            this.resetTime();
          }
        }, 1000);
      }
    );
  }
  resetTime() {
    //重置倒计时
    clearInterval(this.codeTimer);
    const disabledCode = this.state.ischeck ? false : true;
    this.setState({
      disabledCode: disabledCode,
      codetext: "获取验证码",
      timenum: 60,
      lasttime: false,
    });
  }
  submitLogin = () => {
    this.submitCodeLogin();
  };
  submitCodeLogin() {
    const { phone, code, passaction, password } = this.state;
    const { Login } = this.props;
    const urlcode = this.getQueryString("code");
    // 微信code
    const tokencode = urlcode ? urlcode : "";
    if (!tokencode) {
      window.location.href = weixinurl;
    } else if (!/^1\d{10}$/.test(phone)) {
      Toast.info("手机号码不合法");
    } else if (!password && passaction) {
      Toast.info("请输入密码");
    } else {
      const paramdata = {
        phone: phone,
        password: password,
        code: tokencode,
      };
      http("/sale/login", {
        data: paramdata,
      }).then((result) => {
        if (result.code === 0) {
          const data = result.data;
          const value = {
            userId: data.userId,
            token: data.token
          };
          //判断是否登陆
          this.props.Login(value);
          localStorage.setItem("_yzsaleh5user", JSON.stringify(value));
          this.props.ClickPage('index')
          setTimeout(() => {
            this.props.history.replace("/index");
          }, 20)
        } else {
          // Toast.info(result.msg);
          // window.location.href = weixinurl;
          alert("登录失败", result.msg, [
            { text: "确定", onPress: () => (window.location.href = weixinurl) },
          ]);
        }
      });
    }
  }
  clearFromValue(value) {
    switch (value) {
      case 1:
        this.setState({
          phone: "",
        });
        break;
      case 2:
        this.setState({
          password: "",
        });
        break;
    }
  }
  goForgetPassword() {
    this.props.history.push("/forget");
  }
  goCodeLogin() {
    this.setState((prevstate, props) => {
      return {
        passaction: !prevstate.passaction,
      };
    });
  }
  getQueryString(name) {
    //encodeURI decodeURI encodeURIComponent decodeURIComponent
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
      return r[2];
    } else {
      return null;
    }
  }
  render() {
    const {
      phone,
      code,
      disabledCode,
      codetext,
      passaction,
      password,
    } = this.state;
    return (
      <div className="login-page page">
        <Helmet>
          <title>远洲酒店分销登录</title>
        </Helmet>
        <div className="login-banner">
          <img src={loginbanner} className="banner" />
        </div>

        <div className="form-ul">
          <div className="form-li border-1px">
            <span className="form-li-icon phone-icon"></span>
            <input
              type="number"
              className="form-li-input"
              placeholder="请输入手机号码"
              onChange={this.handlePhoneInput}
              value={phone}
              maxLength={11}
              onBlur={this.handleBlur}
            />
            {phone ? (
              <span
                className="form-li-icon close-icon"
                onClick={this.clearFromValue.bind(this, 1)}
              ></span>
            ) : null}
          </div>
          {passaction ? (
            <div className="form-li border-1px">
              <span className="form-li-icon pass-icon"></span>
              <input
                type="password"
                className="form-li-input"
                placeholder="请输入密码"
                onChange={this.handlePasswordInput}
                value={password}
                onBlur={this.handleBlur}
              />
              {password ? (
                <span
                  className="form-li-icon close-icon"
                  onClick={this.clearFromValue.bind(this, 2)}
                ></span>
              ) : null}
            </div>
          ) : (
            <div className="form-li border-1px">
              <span className="form-li-icon code-icon"></span>
              <input
                type="number"
                className="form-li-input li-input-width"
                placeholder="请输入验证码"
                onChange={this.handleCodeInput}
                value={code}
                maxLength={6}
                onBlur={this.handleBlur}
              />
              <span
                className={`form-li-btn ${disabledCode ? "" : "li-btn-active"}`}
                onClick={this.submitCode}
              >
                {codetext}
              </span>
            </div>
          )}
          <div className="submit-btn" onClick={this.submitLogin.bind(this)}>
            登录
          </div>
          <div className="entry-ul">
            <div
              className="entry-li"
              onClick={this.goForgetPassword.bind(this)}
            >
              重置密码
            </div>
            {/* <span className="entry-line"></span>
            <div
              className="entry-li code-entry"
              onClick={this.goCodeLogin.bind(this)}
            >
              {passaction ? "验证码登录" : "密码登录"}
            </div> */}
          </div>
        </div>
      </div>
    );
  }
}
export default Login;
