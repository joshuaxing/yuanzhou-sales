/*
 * @Author: your name
 * @Date: 2020-03-03 11:19:50
 * @LastEditTime: 2020-12-30 14:39:37
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\store\index.js
 */
import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import logger from 'redux-logger'
import reducer from "./reducer";
import service from "./service";
import other from "./other";
const rootReducer = combineReducers({
  reducer,
  service,
  other
});
const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : compose;

const enhancer = composeEnhancers(
    applyMiddleware(thunk, logger)
);
export default function configureStore(initialState) {
  const store = createStore(
    rootReducer,
    initialState,
    compose(
      enhancer,
      window.devToolsExtension ? window.devToolsExtension() : f => f
    )
  );
  return store;
}
