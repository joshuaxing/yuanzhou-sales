/*
 * @Author: your name
 * @Date: 2020-05-13 14:34:23
 * @LastEditTime: 2020-05-13 15:25:35
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-sales\src\pages\ApplyActivity\data.js
 */

const departmentList = [
  {
    label: "前厅部",
    value: 1,
  },
  {
    label: "康体中心",
    value: 2,
  },
  {
    label: "客房部",
    value: 3,
  },
  {
    label: "厨房部",
    value: 4,
  },
  {
    label: "餐饮部",
    value: 5,
  },
  {
    label: "工程部",
    value: 6,
  },
  {
    label: "销售部",
    value: 7,
  },
  {
    label: "财务部",
    value: 8,
  },
  {
    label: "人力资源部",
    value: 9,
  },
  {
    label: "行政办",
    value: 10,
  },
  {
    label: "督导部",
    value: 11,
  },
  {
    label: "亲近服务部",
    value: 12,
  },
  {
    label: "工程部",
    value: 13,
  },
  {
    label: "保安部",
    value: 14,
  },
  {
    label: "财务对账",
    value: 15,
  },
  {
    label: "餐饮收银",
    value: 16,
  },
  {
    label: "房务中心",
    value: 17,
  },
  {
    label: "楼层",
    value: 18,
  },
  {
    label: "洗衣房",
    value: 19,
  },
  {
    label: "PA",
    value: 20,
  },
  {
    label: "管家部",
    value: 21,
  },
  {
    label: "采购部",
    value: 22,
  },
  {
    label: "品质部",
    value: 23,
  },
  {
    label: "发展部",
    value: 24,
  },
  {
    label: "市场部",
    value: 25,
  },
  {
    label: "市场销售部",
    value: 26,
  },
  {
    label: "其他",
    value: 27,
  },
  {
    label: "前厅部",
    value: 28,
  }
];

export default departmentList