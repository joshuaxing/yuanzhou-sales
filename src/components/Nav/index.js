/*
 * @Author: your name
 * @Date: 2020-03-09 11:02:31
 * @LastEditTime: 2020-10-21 16:23:54
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\components\Nav\index.js
 */
import React, {Component} from 'react';
import { withRouter } from "react-router-dom";
import './style.scss';

class Nav extends Component {
  constructor (props){
    super(props);
  }
  clickNav (item) {
    this.props.history.push(item.path);
  }
  render() {
    const { location, navdata} = this.props;
    // console.log(navdata)
    return (
      <div className="nav-component">
        <ul className="nav-ul border-1px">
          {
            navdata.map((item, index) => (
              <li className={`nav-li nav-li-${item.icon} ${location.pathname === item.path ? `nav-li-active active-${item.icon}` : ''}`} onClick={this.clickNav.bind(this, item)} key={String(index)}>
                <span className="nav-li-icon"></span>
                <span className="nav-li-text">{item.name}</span>
              </li>
            ))
          }
        </ul>
      </div>
    )
  }
}
export default withRouter(Nav);
