/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-12-30 13:54:27
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
import BScroll from "better-scroll";
import NotData from "@/components/NotData";
import Divider from "@/components/Divider";
import http from "@/fetch/ajax";
import { Toast } from "antd-mobile";
import { SERVERICON }  from "@/constants/index";


@connect(
  (state) => state.service,
  (dispatch) => bindActionCreators(actions, dispatch)
)
class Order extends React.Component {
  constructor(props) {
    super(props);
    this.hotelwrapper = React.createRef();
  }

  scroll = null;

  state = {
    navindex: '',
    result: [],
    nodata: false,
    page: 1,
    limit: 20,
    totalpage: 1,
    hasmore: false,
  };

  componentDidMount() {
    this.gotData();
    let scroll = (this.scroll = new BScroll(this.hotelwrapper.current, {
      probeType: 1,
      click: true,
      pullUpLoad: {
        threshold: 50,
      },
      pullDownRefresh: {
        threshold: 50,
        stop: 0,
      },
    }));
    scroll.on("pullingUp", () => {
      console.log("我到底了");
      const { totalpage, page } = this.state;
      if (page >= totalpage) {
        this.setState({
          hasmore: true,
        });
        return;
      }
      this.setState(
        {
          page: page + 1,
        },
        () => {
          this.gotData();
        }
      );
    });
  }

  gotData() {
    const user = this.props.user;
    const { page, limit, navindex } = this.state;
    const param = {
      staffid: user.userId,
      pageNum: page,
      pageSize: limit,
      status: navindex
    };
    http("/room/service/staff/order/list", {
      data: param,
    }).then((result) => {
      if (result.code === 0) {
        const data = result.data;
        const total = result.total;
        const totalpage = Math.ceil(total / limit);
        let alldata = this.state.result.concat(data);
        const nodata = alldata.length > 0 ? false : true;
        this.setState({
          result: alldata,
          totalpage: totalpage,
          nodata: nodata,
        });
        if (this.scroll) {
          this.scroll.refresh();
          this.scroll.finishPullUp();
        }
      } else {
        Toast.info(result.msg);
      }
    });
  }

  handleClick(item) {
    this.props.history.push({
      pathname: "/sorderdetail",
      state: { id: item.id }
    });
  }

  clickNav(value) {
    this.setState({
      navindex: value,
      result: [],
      nodata: false,
      hasmore: false,
      page: 1
    }, () => {
      this.gotData()
    });
  }
  computedHeight(vheight) {
    const pageheight = document.body.clientHeight;
    return pageheight - vheight + "px";
  }

  handleSure (id, event) {
    const param = {
      id: id
    };
    http("/room/service/staff/order/confirm", {
      data: param,
    }).then((result) => {
      Toast.info(result.msg);
      if (result.code === 0) {
        this.setState({
          result: [],
          nodata: false,
          hasmore: false,
          page: 1
        }, () => {
          this.gotData()
        });
      }
    });
    event.stopPropagation();
  }

  handleComplete (id, event) {
    const param = {
      id: id,
      service_remark: ''
    };
    http("/room/service/staff/order/finish", {
      data: param,
    }).then((result) => {
      Toast.info(result.msg);
      if (result.code === 0) {
        this.setState({
          result: [],
          nodata: false,
          hasmore: false,
          page: 1
        }, () => {
          this.gotData()
        });
      }
    });
    event.stopPropagation();
  }

  handleSeriveTime(item) {
    const {service_time, service_end_time} = item
    if (service_end_time) {
      const start = this.handleDateStr(service_time);
      const end = this.handleDateStr(service_end_time);
      return `${end.year}-${end.month}-${end.day} ${start.hour}~${end.hour}点`
    } else {
      return service_time
    }
  }

  handleDateStr (value) {
    const arr = value.split(' ');
    const datearr = arr[0].split('-');
    const timearr = arr[1].split(':')
    return {
      year: datearr[0],
      month: this.fillZero(datearr[1]),
      day: this.fillZero(datearr[2]),
      hour: this.fillZero(timearr[0]),
      minute: this.fillZero(timearr[1]),
      second: this.fillZero(timearr[2])
    }
  }

  fillZero(value) {
    let num = parseInt(value);
    return num < 10 ? "0" + num : num;
  }

  render() {
    const { result, nodata, hasmore, navindex } = this.state;
    return (
      <div className="hotel-page page">
        <Helmet>
          <title>客房服务单</title>
        </Helmet>
        <div className="type-navs">
          <div
            className={`type-nav ${navindex === '' ? "active" : ""}`}
            onClick={this.clickNav.bind(this, '')}
          >
            全部
          </div>
          <div
            className={`type-nav ${navindex === 2 ? "active" : ""}`}
            onClick={this.clickNav.bind(this, 2)}
          >
            待服务
          </div>
        </div>
        <div
          className="order-ul-wrapper"
          ref={this.hotelwrapper}
          style={{ height: this.computedHeight(95) }}
        >
          <ul className="order-ul">
            {
              result.map((item) => (
                <li
                  className="order-li model-shodow"
                  onClick={this.handleClick.bind(this, item)}
                  key={item.id}
                >
                  <div className="order-li-title">
                    <div className="title-name">房间号: <span className="title-name-span">{item.roomno}</span></div>
                    <div className={`title-status ${item.status === 1 ? 'active' : ''}`}>{item.status === 1 ? '待确认' : item.status === 2 ? '待服务' : item.status === 3 ? '服务完成' : item.status === 4 ? '已取消' : ''}</div>
                  </div>
                  <div className="order-li-content">
                    <img
                      src={SERVERICON + item.icon_url}
                      className="order-li-images"
                    />
                    <div className="content">
                      <div className="room">{item.categoryname}</div>
                      <div className="time">服务时间：{this.handleSeriveTime(item)}</div>
                    </div>
                  </div>
                  <div className="btns">
                    {
                      item.status === 1 && ( <div className="btn" onClick={this.handleSure.bind(this, item.id)}>确认服务</div>)
                    }
                    {
                      item.status === 2 && ( <div className="btn" onClick={this.handleComplete.bind(this, item.id)}>服务完成</div>)
                    }
                  </div>
                </li>
              ))
            }
            
            {/* <li
              className="order-li model-shodow"
              onClick={this.handleClick.bind(this)}
            >
              <div className="order-li-title">
                <div className="title-name">房间号: <span className="title-name-span">1号楼-1703</span></div>
                <div className={`title-status active`}>待入住</div>
              </div>
              <div className="order-li-content">
                <img
                  src="https://pic.snhotelsgroup.com/common/hotel_001.png"
                  className="order-li-images"
                />
                <div className="content">
                  <div className="room">接车服务</div>
                  <div className="time">服务时间：2020-12-30 13:30</div>
                </div>
              </div>
              <div className="btns">
                <div className="btn">服务完成</div>
                <div className="btn active">接单</div>
                <div className="btn active2">修改</div>
              </div>
            </li> */}

            {nodata && <NotData></NotData>}
            {hasmore && <Divider></Divider>}
          </ul>
        </div>
      </div>
    );
  }
}
export default Order;
