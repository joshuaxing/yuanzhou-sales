"use strict";
const path = require("path");
const utils = require("./utils");
const webpack = require("webpack");
const config = require("../config");
const merge = require("webpack-merge");
const baseWebpackConfig = require("./webpack.base.conf");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

// console.log(process.env.npm_config_build)
const wxappid = process.env.npm_config_build ? config.build.wxappid : config.dev.wxappid;

const wxurl = process.env.npm_config_build ? config.build.wxurl : config.dev.wxurl;

const resourceurl = process.env.npm_config_build ? config.build.resourceurl : config.dev.resourceurl;

const qlogo = process.env.npm_config_build ? config.build.qlogo : config.dev.qlogo;

const nodeenv = process.env.npm_config_build ? 'build:pro' : 'build:dev';

const staticurl = process.env.npm_config_build ? config.build.staticurl : config.dev.staticurl;

console.log('nodeenv', nodeenv)

console.log('appid', wxappid)

console.log('wxurl', wxurl)

console.log('resourceurl', resourceurl)

console.log('staticurl', staticurl)

const webpackConfig = merge(baseWebpackConfig, {
  module: {
    rules: utils.styleLoaders({
      sourceMap: config.build.productionSourceMap,
      usePostCSS: true,
    }),
  },
  mode: config.build.mode,
  devtool: config.build.productionSourceMap ? config.build.devtool : false,
  output: {
    path: config.build.assetsRoot,
    filename: utils.assetsPath("js/[name].[chunkhash:8].js"),
    chunkFilename: utils.assetsPath("js/[id].[chunkhash:8].js"),
  },
  optimization: {
    splitChunks: {
      chunks: "all",
      minSize: 30000,
      minChunks: 1,
      maxAsyncRequests: 5,
      maxInitialRequests: 3,
      automaticNameDelimiter: "~",
      name: true,
      cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          chunks: "initial",
          reuseExistingChunk: true,
          priority: -10,
        },
      },
    },
    runtimeChunk: {
      name: "runtime",
    },
    minimizer: [
      new OptimizeCSSAssetsPlugin({
        cssProcessorOptions: config.build.productionSourceMap
          ? { safe: true, map: { inline: false } }
          : { safe: true },
      }),
      // new UglifyJsPlugin({
      //   uglifyOptions: {
      //     warnings: false,
      //     compress: {
      //       drop_debugger: true,
      //       drop_console: false,
      //     },
      //   },
      //   cache: false,
      //   parallel: true,
      //   sourceMap: config.build.productionSourceMap,
      // }),
    ],
  },
  plugins: [
    
    // Add DefinePlugin
    new webpack.DefinePlugin({
      wxappid: JSON.stringify(wxappid),
      wxurl: JSON.stringify(wxurl),
      resourceurl: JSON.stringify(resourceurl),
      qlogo: JSON.stringify(qlogo),
      nodeenv: JSON.stringify(nodeenv),
      staticurl: JSON.stringify(staticurl)
    }),

    new CleanWebpackPlugin(),
    // extract css into its own file
    // new MiniCssExtractPlugin({
    //   filename: utils.assetsPath('css/[name].[contenthash].css'),
    //   chunkFilename: utils.assetsPath('css/[id].css')
    // }),
    new MiniCssExtractPlugin({
      filename: utils.assetsPath("css/[name].[hash:8].css"),
      chunkFilename: utils.assetsPath("css/[id].[hash:8].css"),
    }),
    // generate dist index.html with correct asset hash for caching.
    // you can customize output by editing /index.html
    // see https://github.com/ampedandwired/html-webpack-plugin
    new HtmlWebpackPlugin({
      filename: config.build.index,
      template: "index.html",
      inject: true,
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeAttributeQuotes: true,
        // more options:
        // https://github.com/kangax/html-minifier#options-quick-reference
      },
      // necessary to consistently work with multiple chunks via CommonsChunkPlugin
      chunksSortMode: "dependency",
      favicon: path.resolve('favicon.ico')
    }),
    // keep module.id stable when vendor modules does not change
    new webpack.HashedModuleIdsPlugin(),

    // copy custom static assets
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, "../static"),
        to: config.build.assetsSubDirectory,
        ignore: [".*"],
      },
    ]),
  ],
});

if (config.build.productionGzip) {
  const CompressionWebpackPlugin = require("compression-webpack-plugin");

  webpackConfig.plugins.push(
    new CompressionWebpackPlugin({
      asset: "[path].gz[query]",
      algorithm: "gzip",
      test: new RegExp(
        "\\.(" + config.build.productionGzipExtensions.join("|") + ")$"
      ),
      threshold: 10240,
      minRatio: 0.8,
    })
  );
}

if (config.build.bundleAnalyzerReport) {
  const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
    .BundleAnalyzerPlugin;
  webpackConfig.plugins.push(new BundleAnalyzerPlugin());
}

module.exports = webpackConfig;
