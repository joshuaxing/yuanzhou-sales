/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-05-27 10:03:35
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import { Toast } from "antd-mobile";
import http from "@/fetch";
import * as actions from "@/store/actions";
import loginbanner from "./loginbanner.jpg";

@connect(
  state => state,
  dispatch => bindActionCreators(actions, dispatch)
)
class Forget extends React.Component {
  codeTimer = null
  state = {
    phone: "",
    code: "",
    password: "",
    repassword: "",
    lasttime: false,
    ischeck: false,
    disabledCode: true,
    codetext: "获取验证码",
    timenum: 60
  };
  componentDidMount() {}

  handlePhoneInput = e => {
    const value = e.target.value;
    if (!/^1[3456789]\d{9}$/.test(value)) {
      this.setState({
        ischeck: false,
        phone: value,
        disabledCode: true
      });
    } else {
      //手机号正确
      if (!this.state.lasttime) {
        this.setState({
          disabledCode: false,
          ischeck: true,
          phone: value
        });
      } else {
        //倒计时中
        this.setState({
          disabledCode: true,
          ischeck: true,
          phone: value
        });
      }
    }
  };

  submitCode = () => {
    const { phone, disabledCode } = this.state;
    if (disabledCode) {
      return;
    }
    const param = {
      phone: phone
    }
    //获取验证码
    http("/sale/login/verifycode", {
      data: param
    }).then(result => {
      if (result.code === 0) {
        //倒计时
        this.countTime();
      }
      Toast.info(result.msg);
    });
  };

  handleCodeInput = e => {
    const value = e.target.value;
    this.setState({
      code: value
    });
  };

  handlePasswordInput = e => {
    const value = e.target.value;
    this.setState({
      password: value
    });
  };

  handleRePasswordInput = e => {
    const value = e.target.value;
    this.setState({
      repassword: value
    });
  };

  countTime() {
    //倒计时
    this.setState(
      {
        codetext: this.state.timenum + "s",
        disabledCode: true,
        lasttime: true
      },
      () => {
        this.codeTimer && clearInterval(this.codeTimer);
        this.codeTimer = setInterval(() => {
          this.setState((state, props) => ({
            timenum: state.timenum - 1,
            codetext: state.timenum + "s"
          }));
          if (this.state.timenum <= 0) {
            this.resetTime();
          }
        }, 1000);
      }
    );
  }
  
  resetTime() {
    //重置倒计时
    clearInterval(this.codeTimer);
    const disabledCode = this.state.ischeck ? false : true;
    this.setState({
      disabledCode: disabledCode,
      codetext: "获取验证码",
      timenum: 60,
      lasttime: false
    });
  }
  submitLogin() {
    this.submitCodeLogin();
  }
  submitCodeLogin() {
    const { phone, code, password, repassword} = this.state;
    let reg = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,12}$/;
    if (!/^1[3456789]\d{9}$/.test(phone)) {
      Toast.info('手机号码不合法')
    } else if (!code) {
      Toast.info('请输入验证码')
    } else if (!reg.test(password)) {
      Toast.info('密码为6-12位数字和字母组合')
    } else if (password !== repassword) {
      Toast.info('确认密码和设置密码不一致')
    } else {
      const paramdata = {
        phone: phone,
        verifycode: code,
        password: password
      };
      http("/sale/login/changepwd", {
        data: paramdata
      }).then(result => {
        if (result.code === 0) {
          Toast.info("修改成功，快去登录吧")
          this.goBack()
        } else {
          Toast.info(result.msg)
        }
      });
    }
  }
  clearFromValue(value) {
    switch (value) {
      case 1:
        this.setState({
          phone: ""
        });
        break;
      case 2:
        this.setState({
          password: ""
        });
        break;
      case 3:
        this.setState({
          repassword: ""
        });
        break;
    }
  }

  goBack () {
    this.props.history.push('/login');
  }
  
  handleBlur () {
    window.scrollTo(0,0)
  }
  
  render() {
    const {
      phone,
      code,
      disabledCode,
      codetext,
      repassword,
      password
    } = this.state;
    return (
      <div className="forget-page page">
        <Helmet>
          <title>重置密码</title>
        </Helmet>
        <div className="login-banner">
          <img src={loginbanner} className="banner" />
        </div>
        <div className="form-ul">
          <div className="form-li border-1px">
            <span className="form-li-icon phone-icon"></span>
            <input
              type="number"
              className="form-li-input"
              placeholder="请输入手机号码"
              onChange={this.handlePhoneInput}
              value={phone}
              maxLength={11}
              onBlur={this.handleBlur}
            />
            {phone ? (
              <span
                className="form-li-icon close-icon"
                onClick={this.clearFromValue.bind(this, 1)}
              ></span>
            ) : null}
          </div>
          <div className="form-li border-1px">
            <span className="form-li-icon code-icon"></span>
            <input
              type="number"
              className="form-li-input li-input-width"
              placeholder="请输入验证码"
              onChange={this.handleCodeInput}
              value={code}
              maxLength={6}
              onBlur={this.handleBlur}
            />
            <span
              className={`form-li-btn ${disabledCode ? "" : "li-btn-active"}`}
              onClick={this.submitCode}
            >
              {codetext}
            </span>
          </div>
          <div className="form-li border-1px">
            <span className="form-li-icon pass-icon"></span>
            <input
              type="password"
              className="form-li-input"
              placeholder="请设置密码"
              onChange={this.handlePasswordInput}
              value={password}
              onBlur={this.handleBlur}
            />
            {password ? (
                <span
                  className="form-li-icon close-icon"
                  onClick={this.clearFromValue.bind(this, 2)}
                ></span>
              ) : null}
          </div>
          <div className="form-li border-1px">
            <span className="form-li-icon pass-icon"></span>
            <input
              type="password"
              className="form-li-input"
              placeholder="请确认密码"
              onChange={this.handleRePasswordInput}
              value={repassword}
              onBlur={this.handleBlur}
            />
            {repassword ? (
                <span
                  className="form-li-icon close-icon"
                  onClick={this.clearFromValue.bind(this, 3)}
                ></span>
              ) : null}
          </div>
          <div className="submit-btn" onClick={this.submitLogin.bind(this)}>
            确 认
          </div>
          <div className="entry-ul">
            <div className="entry-li" onClick={this.goBack.bind(this)}>返回登录</div>
          </div>
        </div>
      </div>
    );
  }
}
export default Forget;
