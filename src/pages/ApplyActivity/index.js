/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-12-29 10:02:08
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import { Toast, Picker, Modal } from "antd-mobile";
import http from "@/fetch";
import * as actions from "@/store/actions";
import loginbanner from "./loginbanner.jpg";

@connect(
  (state) => state.reducer,
  (dispatch) => bindActionCreators(actions, dispatch)
)
class ApplyActivity extends React.Component {
  codeTimer = null;
  state = {
    phone: "",
    code: "",
    name: "",
    departmentValue: [],
    departmentid: "",
    departmentname: "",
    password: "",
    repassword: "",
    lasttime: false,
    ischeck: false,
    disabledCode: true,
    codetext: "获取验证码",
    timenum: 60,
    departmentList: [],
  };
  componentDidMount() {
    // console.log(departmentData)
    // this.setState({
    //   departmentList: departmentData,
    // });
    const id = this.getQueryString("id");
    if (!id) {
      Modal.alert("二维码参数错误", "二维码参数错误，请重新生成二维码", [
        { text: "确定", onPress: () => this.props.history.push("/login") },
      ]);
    }
  }

  handlePhoneInput = (e) => {
    const value = e.target.value;
    if (!/^1\d{10}$/.test(value)) {
      this.setState({
        ischeck: false,
        phone: value,
        disabledCode: true,
      });
    } else {
      //手机号正确
      if (!this.state.lasttime) {
        this.setState({
          disabledCode: false,
          ischeck: true,
          phone: value,
        });
      } else {
        //倒计时中
        this.setState({
          disabledCode: true,
          ischeck: true,
          phone: value,
        });
      }
    }
  };

  submitCode = () => {
    const { phone, disabledCode } = this.state;
    if (disabledCode) {
      return;
    }
    const param = {};
    //获取验证码
    http("/user/sale/apply/verify", {
      data: phone,
    }).then((result) => {
      if (result.code === 0) {
        //倒计时
        this.countTime();
      }
      Toast.info(result.msg);
    });
  };

  handleCodeInput = (e) => {
    const value = e.target.value;
    this.setState({
      code: value,
    });
  };

  handleNameInput = (e) => {
    const value = e.target.value;
    this.setState({
      name: value,
    });
  };
  handleRePasswordInput = (e) => {
    const value = e.target.value;
    this.setState({
      repassword: value,
    });
  };
  handlePasswordInput = (e) => {
    const value = e.target.value;
    this.setState({
      password: value,
    });
  };

  hanldeOk = (v) => {
    const departmentList = this.state.departmentList;
    const filteritem = departmentList.filter((item) => item.value === v[0]);
    this.setState({
      departmentid: v[0],
      departmentname: filteritem[0].label,
      departmentValue: v,
    });
  };
  countTime() {
    //倒计时
    this.setState(
      {
        codetext: this.state.timenum + "s",
        disabledCode: true,
        lasttime: true,
      },
      () => {
        this.codeTimer && clearInterval(this.codeTimer);
        this.codeTimer = setInterval(() => {
          this.setState((state, props) => ({
            timenum: state.timenum - 1,
            codetext: state.timenum + "s",
          }));
          if (this.state.timenum <= 0) {
            this.resetTime();
          }
        }, 1000);
      }
    );
  }

  resetTime() {
    //重置倒计时
    clearInterval(this.codeTimer);
    const disabledCode = this.state.ischeck ? false : true;
    this.setState({
      disabledCode: disabledCode,
      codetext: "获取验证码",
      timenum: 60,
      lasttime: false,
    });
  }
  submitLogin() {
    this.submitCodeLogin();
  }
  submitCodeLogin() {
    const { phone, code, name, departmentid, password, repassword } = this.state;
    let reg = /^[0-9A-Za-z]{6,12}$/;

    const hotelid = this.getQueryString("id") ? this.getQueryString("id") : '';
    if (!hotelid) {
      Modal.alert("二维码参数错误", "二维码参数错误，请重新扫描", [
        { text: "确定", onPress: () => this.props.history.push("/login") },
      ]);
      return;
    }

    if (!/^1\d{10}$/.test(phone)) {
      Toast.info("手机号码不合法");
    } else if (!code) {
      Toast.info("请输入验证码");
    } else if (!name) {
      Toast.info("请输入姓名");
    }  else if (!reg.test(password)) {
      Toast.info('密码为6-12位数字、字母或数字和字母组合')
    } else if (password !== repassword) {
      Toast.info('确认密码和设置密码不一致')
    } else {
      const paramdata = {
        phone: phone,
        verifycode: code,
        name: name,
        departmentid: 0,
        activityid: 0,
        password: password,
        hotelid: parseInt(hotelid)
      };
      http("/user/sale/apply/save", {
        data: paramdata,
      }).then((result) => {
        if (result.code === 0) {
          Toast.info("注册成功，快去登陆吧");
          setTimeout(() => {
            this.goBack();
          }, 1500);
        } else {
          Toast.info(result.msg);
        }
      });
    }
  }
  clearFromValue(value) {
    switch (value) {
      case 1:
        this.setState({
          phone: ""
        });
        break;
      case 2:
        this.setState({
          name: ""
        });
        break;
      case 3:
        this.setState({
          departmentid: ""
        });
        break;
      case 4:
        this.setState({
          password: ""
        });
        break;
      case 5:
        this.setState({
          repassword: ""
        });
        break;
    }
  }

  goBack() {
    this.props.history.push("/login");
  }
  handleBlur() {
    window.scrollTo(0, 0);
  }
  getQueryString(name) {
    //encodeURI decodeURI encodeURIComponent decodeURIComponent
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) {
      return r[2];
    } else {
      return null;
    }
  }
  render() {
    const {
      phone,
      code,
      disabledCode,
      codetext,
      name,
      password,
      repassword,
      departmentname,
      departmentList,
      departmentValue,
      departmentid
    } = this.state;
    return (
      <div className="apply-page page">
        <Helmet>
          <title>员工注册</title>
        </Helmet>
        <div className="login-banner">
          <img src={loginbanner} className="banner" />
        </div>
        <div className="form-ul">
          <div className="form-li border-1px">
            <span className="form-li-icon phone-icon"></span>
            <input
              type="number"
              className="form-li-input"
              placeholder="请输入手机号码"
              onChange={this.handlePhoneInput}
              value={phone}
              maxLength={11}
              onBlur={this.handleBlur}
            />
            {phone ? (
              <span
                className="form-li-icon close-icon"
                onClick={this.clearFromValue.bind(this, 1)}
              ></span>
            ) : null}
          </div>
          <div className="form-li border-1px">
            <span className="form-li-icon code-icon"></span>
            <input
              type="number"
              className="form-li-input li-input-width"
              placeholder="请输入验证码"
              onChange={this.handleCodeInput}
              value={code}
              maxLength={6}
              onBlur={this.handleBlur}
            />
            <span
              className={`form-li-btn ${disabledCode ? "" : "li-btn-active"}`}
              onClick={this.submitCode}
            >
              {codetext}
            </span>
          </div>
          <div className="form-li border-1px">
            <span className="form-li-icon user-icon"></span>
            <input
              type="text"
              className="form-li-input"
              placeholder="请输入姓名"
              onChange={this.handleNameInput}
              value={name}
              onBlur={this.handleBlur}
            />
            {name ? (
              <span
                className="form-li-icon close-icon"
                onClick={this.clearFromValue.bind(this, 2)}
              ></span>
            ) : null}
          </div>

          <div className="form-li border-1px">
            <span className="form-li-icon pass-icon"></span>
            <input
              type="password"
              className="form-li-input"
              placeholder="设置密码"
              onChange={this.handlePasswordInput}
              value={password}
              onBlur={this.handleBlur}
            />
            {password ? (
              <span
                className="form-li-icon close-icon"
                onClick={this.clearFromValue.bind(this, 4)}
              ></span>
            ) : null}
          </div>

          <div className="form-li border-1px">
            <span className="form-li-icon pass-icon"></span>
            <input
              type="password"
              className="form-li-input"
              placeholder="确认密码"
              onChange={this.handleRePasswordInput}
              value={repassword}
              onBlur={this.handleBlur}
            />
            {repassword ? (
              <span
                className="form-li-icon close-icon"
                onClick={this.clearFromValue.bind(this, 5)}
              ></span>
            ) : null}
          </div>

          {/* this.hanldeOk.bind(this, v) */}

          {/* <Picker data={departmentList} cols={1} onOk = {v => {this.hanldeOk(v)}} value={departmentValue}>
            <div className="form-li border-1px">
              <span className="form-li-icon department-icon"></span>
              <input
                type="text"
                className="form-li-input"
                placeholder="请选择部门"
                value={departmentname}
                disabled
              />
              {departmentid ? (
                <span
                  className="form-li-icon close-icon"
                  onClick={this.clearFromValue.bind(this, 3)}
                ></span>
              ) : null}
            </div>
          </Picker> */}

          <div className="submit-btn" onClick={this.submitLogin.bind(this)}>
            确 认
          </div>
          <div className="entry-ul">
            <div className="entry-li" onClick={this.goBack.bind(this)}>
              前往登录页面
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default ApplyActivity;
