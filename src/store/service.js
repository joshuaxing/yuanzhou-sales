/*
 * @Author: your name
 * @Date: 2020-12-08 13:58:35
 * @LastEditTime: 2020-12-08 17:02:28
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-sales\src\store\service.js
 */
const initialState = {
    user: {
      userId: 0,
      token: ''
    }
  };
  
  export default function login(state = initialState, action) {
    const { type, payload } = action;
    switch (type) {
      case 'Login2':
        // console.log('Login2')
        return {
          ...state,
          user: payload
        }
      default:
        return state;
    }
  }