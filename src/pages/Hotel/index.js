/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-12-08 15:54:53
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
import BScroll from "better-scroll";
import http from "@/fetch";
import NotData from "@/components/NotData";
import Divider from "@/components/Divider";
import BackBar from "@/components/BackBar";
@connect(
  state => state.reducer,
  dispatch => bindActionCreators(actions, dispatch)
)
class Hotel extends React.Component {

  constructor (props) {
    super(props);
    this.hotelwrapper = React.createRef();
  }

  scroll = null

  state = {
    result: [],
    nodata: false,
    page: 1,
    limit: 20,
    totalpage: 1,
    hasmore: false
  };

  componentDidMount() {
    this.gotData();
    let scroll = this.scroll = new BScroll(this.hotelwrapper.current, {
      probeType: 1,
      click: true,
      pullUpLoad: {
        threshold: 50
      },
      pullDownRefresh: {
        threshold: 50,
        stop: 0
      }
    });
    scroll.on('pullingUp', () => {
      console.log('我到底了')
      const { totalpage, page } = this.state;
      if (page >= totalpage) {
        this.setState({
          hasmore: true
        });
        return;
      }
      this.setState(
        {
          page: page + 1
        },
        () => {
          this.gotData();
        }
      ); 
    })
  }

  gotData() {
    const user = this.props.user;
    const {page, limit} = this.state;
    const param = {
      saleid: user.userId,
      pageNum: page,
      pageSize: limit
    };
    http("/sale/user/roomlist", {
      data: param
    }).then(result => {
      if (result.code === 0) {
        const data = result.data;
        const total = result.total;
        const totalpage = Math.ceil(total / limit);
        let alldata = this.state.result.concat(data);
        const nodata = alldata.length > 0 ? false : true
        this.setState({
          result: alldata,
          totalpage: totalpage,
          nodata: nodata
        });
        if (this.scroll) {
          this.scroll.refresh();
          this.scroll.finishPullUp();
        }
      } else {
        Toast.info(result.msg);
      }
    });
  }
  
  handleClick (item) {
    this.props.history.push({pathname:'/sharehotel',state:{ id : item.id } });
  }

  computedHeight(vheight) {
    const pageheight = document.body.clientHeight;
    return pageheight - vheight + "px";
  }
  render() {
    const { result, nodata, hasmore} = this.state;
    return (
      <div className="hotel-page page">
        <Helmet>
          <title>订房分享</title>
        </Helmet>
        <BackBar></BackBar>
        <div
          className="hotel-ul-wrapper"
          ref={this.hotelwrapper}
          style={{ height: this.computedHeight(0)}}
        >
          <ul className="hotel-ul">
            {
              result.map((item, index) => (
                <li className="hotel-li" key={String(index)}>
                  <div className="hotel-li-image">
                    <img src={item.imageurl} className="li-image" />
                  </div>
                  <div className="hotel-li-content">
                    <span className="hotel-name">{item.hotelname}</span>
                    <span className="hotel-room">{item.roomname}</span>
                    <span className="hotel-profit">预计佣金：{item.commissiontype === 2 ? item.commission_amount + '元' : "订单金额" + (item.commission_amount*100).toFixed(2)+'%' }</span>
                  </div>
                  <div className="store-btn" onClick={this.handleClick.bind(this, item)}>我要分享</div>
                </li>
              ))
            }
            {
              nodata && <NotData></NotData>
            }
            {
              hasmore && <Divider></Divider>
            }
          </ul>
        </div>
      </div>
    );
  }
}
export default Hotel;
