/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-04-13 11:54:46
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
import nofoud from "./nofoud.png";
@connect(
  state => state.reducer,
  dispatch => bindActionCreators(actions, dispatch)
)
class NOMATCH extends React.Component {

  state = {
    
  }
  componentDidMount() {}

  componentWillUnmount () {}

  handleBack () {
    this.props.history.push('/')
  }
  render() {

    return (
      <div className="nofound-page page">
        <Helmet>
          <title>页面404</title>
        </Helmet>
        <div className="nofound-content">
          <img src={nofoud} className="nofound-icon"/>
          <div className="nofound-txt">页面未找到</div>
          <div className="nofound-btn" onClick={this.handleBack.bind(this)}>返回</div>
        </div>
      </div>
    );
  }
}
export default NOMATCH;
