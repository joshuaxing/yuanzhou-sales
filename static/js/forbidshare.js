/*
 * @Author: your name
 * @Date: 2020-04-21 11:52:34
 * @LastEditTime: 2020-04-21 11:52:57
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-sales\static\js\forbidshare.js
 */
function onBridgeReady() {
  WeixinJSBridge.call("hideOptionMenu");
}

if (typeof WeixinJSBridge == "undefined") {
  if (document.addEventListener) {
    document.addEventListener("WeixinJSBridgeReady", onBridgeReady, false);
  } else if (document.attachEvent) {
    document.attachEvent("WeixinJSBridgeReady", onBridgeReady);
    document.attachEvent("onWeixinJSBridgeReady", onBridgeReady);
  }
} else {
  onBridgeReady();
}
