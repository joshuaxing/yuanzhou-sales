/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-10-19 09:19:21
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
import bg from "./bg.jpg";
import logo from "./logo.png";
import title from "./gold-title.png";
import headicon from "./headicon.png";
@connect(
  (state) => state.reducer,
  (dispatch) => bindActionCreators(actions, dispatch)
)
class CanvasPage extends React.Component {
  static defaultProps = {
    canvaswidth: 750, // 画布宽度
    canvasheight: 1334, // 画布高度
    views: [
      {
        type: "image",
        path: bg,
        top: 0,
        left: 0,
        width: 750,
        height: 1334,
      },
      {
        type: "image",
        path: logo,
        top: 80,
        left: 40,
        width: 220,
        height: 35
      },
      {
        type: "image",
        path: title,
        top: 180,
        left: 105,
        width: 540,
        height: 112
      },
      {
        type: "text",
        content: "赠送300元大礼包",
        fontSize: "28px Arial",
        color: "#8A6543",
        textAlign: "center",
        top: 352,
        left: 0
      },
      {
        type: "rect",
        background: '#fff',
        top: 580,
        left: 247,
        width: 256,
        height: 256
      },
      {
        type: "text",
        content: "长按立即购买金卡会员",
        fontSize: "14px Arial",
        color: "#ffffff",
        textAlign: "center",
        top: 876,
        left: 0
      },
      {
        type: "image",
        path: headicon,
        top: 1000,
        left: 341,
        width: 68,
        height: 68
      },
      {
        type: "text",
        content: `来自小型的分享`,
        fontSize: "14px Arial",
        color: "#ffffff",
        textAlign: "center",
        top: 1088,
        left: 0
      },
    ]
  };

  state = {
    per: 0.5,
    nickname: '小型'
  }

  constructor(props) {
    super(props);
    this.canvas = React.createRef();
  }

  componentDidMount() {
    const per = this.gotper();
    this.setState({
      per: per
    }, () => {
      this.initCanvas();
    })

  }

  componentWillUnmount() {}

  initCanvas() {
    this.canvasdom = this.canvas.current;
    if (this.canvasdom.getContext) {
      this.ctx = this.canvasdom.getContext("2d");
      this.getImagesInfo()

    }
  }
  startPainting(tempFileList) {

    const { views }= this.props;

    for (let i = 0, imageIndex = 0; i < views.length; i++) {
      if (views[i].type === 'image') {
        this.drawImage({
          ...views[i],
          img: tempFileList[imageIndex]
        })
        imageIndex++
      } else if (views[i].type === 'text') {
        this.drawText(views[i])
      } else if (views[i].type === 'rect') {
        this.drawRect({
          ...views[i]
        })
      }
    }

    setTimeout(() => {
      const url = this.canvasdom.toDataURL("image/png");
      
    }, 100)
  }

  // 获取图片信息
  getImagesInfo () {
    let imageList = [];
    const views = this.props.views;
    for (let i = 0; i < views.length; i++) {
      if (views[i].type === 'image') {
        imageList.push(this.getImageInfo(views[i].path))
      }
    }

    let loadTask = []

    for (let i = 0; i < Math.ceil(imageList.length / 8); i++) {
      
      loadTask.push(new Promise((resolve, reject) => {
        Promise.all(imageList.splice(i * 8, 8)).then(res => {
          resolve(res)
        }).catch(res => {
          reject(res)
        })
      }))
    }
    Promise.all(loadTask).then(res => {
      let tempFileList = []
      for (let i = 0; i < res.length; i++) {
        tempFileList = tempFileList.concat(res[i])
      }
      this.startPainting(tempFileList)
    })

  }

  // 写字
  drawText (params) {
    const { canvaswidth } = this.props;
    const per = this.state.per;
    const {
      MaxLineNumber = 2,
      breakWord = true,
      color = '#000000',
      content = '',
      fontSize = "14px sans-serif",
      top = 0,
      left = 0,
      lineHeight = 20,
      textAlign,
      width,
      bolder = false,
      textDecoration = 'none'
    } = params

    this.ctx.save()
    this.ctx.textBaseline = 'top'
    this.ctx.fillStyle = color;
    this.ctx.font = fontSize;
    this.ctx.textAlign = textAlign;
    if (textAlign === 'center') {
      this.ctx.fillText(content, canvaswidth*per/2, top*per)
    } else {
      if (!breakWord) {
        this.ctx.fillText(content, left*per, top*per)
        this.drawTextLine(left, top, textDecoration, color, fontSize, content)
      } else {
        let fillText = ''
        let fillTop = top
        let lineNum = 1
        for (let i = 0; i < content.length; i++) {
          fillText += [content[i]]
          if (this.ctx.measureText(fillText).width > width) {
            // console.log(lineNum)
            if (lineNum === MaxLineNumber) {
              if (i !== content.length) {
                fillText = fillText.substring(0, fillText.length - 1) + '...'
                this.ctx.fillText(fillText, left, fillTop)
                this.drawTextLine(left, fillTop, textDecoration, color, fontSize, fillText)
                fillText = ''
                break
              }
            }
            this.ctx.fillText(fillText, left, fillTop)
            this.drawTextLine(left, fillTop, textDecoration, color, fontSize, fillText)
            fillText = ''
            fillTop += lineHeight
            lineNum ++
          }
        }
        this.ctx.fillText(fillText, left, fillTop)
        this.drawTextLine(left, fillTop, textDecoration, color, fontSize, fillText)
      }
    }
    
    this.ctx.restore()

    if (bolder) {
      this.drawText({
        ...params,
        left: left + 0.3,
        top: top + 0.3,
        bolder: false,
        textDecoration: 'none' 
      })
    }

  }
  // 画线
  drawTextLine ({left, top, textDecoration, color, fontSize, content}) {
    if (textDecoration === 'underline') {
      this.drawRect({
        background: color,
        top: top + fontSize * 1.2,
        left: left - 1,
        width: this.ctx.measureText(content).width + 3,
        height: 1
      })
    } else if (textDecoration === 'line-through') {
      this.drawRect({
        background: color,
        top: top + fontSize * 0.6,
        left: left - 1,
        width: this.ctx.measureText(content).width + 3,
        height: 1
      })
    }
  }
  // 画矩形
  drawRect ({background, top, left, width, height}) {
    const per = this.state.per;
    this.ctx.save()
    this.ctx.fillStyle = background;
    this.ctx.fillRect(left*per, top*per, width*per, height*per)
    this.ctx.restore()
  }

  // 画图
  drawImage({img, left, top, width, height}) {
    const per = this.state.per
    this.ctx.save();
    this.ctx.drawImage(img, left*per, top*per, width*per, height*per);
    this.ctx.restore();
  }

  // 返回图片对象
  getImageInfo (path) {
    return new Promise((resolve, reject) => {
      let img = new Image();
      img.setAttribute("crossOrigin", "Anonymous");
      img.onload = function () {
        resolve(img)
      }
      img.error = function () {
        resolve(img)
      }
      img.src = path;
    })
  }

  // 获取dpr
  dpr() {
    if (window.devicePixelRatio && window.devicePixelRatio > 1) {
      return window.devicePixelRatio;
    }
    return 1;
  }
  // 适配
  gotper() {
    const systemWidth =
      document.documentElement.clientWidth || document.body.clientWidth;
    const scale = (systemWidth / 750).toFixed(2);
    return scale;
  }

  render() {
    const { canvaswidth, canvasheight } = this.props;
    const { per } = this.state;
    return (
      <div className="canvas-page page">
        <Helmet>
          <title>Canvas</title>
        </Helmet>
        <canvas
          width={canvaswidth * per}
          height={canvasheight * per}
          ref={this.canvas}
        ></canvas>
      </div>
    );
  }
}
export default CanvasPage;
