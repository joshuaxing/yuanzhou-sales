/*
 * @Author: your name
 * @Date: 2020-03-03 11:22:48
 * @LastEditTime: 2020-12-30 13:58:55
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\router\index.js
 */
import Loadable from 'react-loadable';
import { createBrowserHistory } from 'history';
import MainLayout from '@/layouts/MainLayout';
import Loading from '@/components/Loading';
//createHashHistory
const Index = Loadable({loader: () => import('@/pages/Index'),loading: Loading});
const Record = Loadable({loader: () => import('@/pages/Record'),loading: Loading});
const User = Loadable({loader: () => import('@/pages/User'),loading: Loading});
// 排行榜
const RankingList = Loadable({loader: () => import('@/pages/RankingList'),loading: Loading});
// 我的佣金
const Brokerage = Loadable({loader: () => import('@/pages/Brokerage'),loading: Loading});
// 佣金提现
const Withdraw = Loadable({loader: () => import('@/pages/Withdraw'),loading: Loading});
// 常见问题
const CommonIssue = Loadable({loader: () => import('@/pages/CommonIssue'),loading: Loading});
// 常见详情
const IssueOne = Loadable({loader: () => import('@/pages/IssueOne'),loading: Loading});
// 分销酒店
const Hotel = Loadable({loader: () => import('@/pages/Hotel'),loading: Loading});
// 公众号分销
const ShareGZH = Loadable({loader: () => import('@/pages/ShareGZH'),loading: Loading});
// 金卡分销
const ShareGold = Loadable({loader: () => import('@/pages/ShareGold'),loading: Loading});
// 会员分销
const ShareWX = Loadable({loader: () => import('@/pages/ShareWX'),loading: Loading});
// 酒店分销
const ShareHotel = Loadable({loader: () => import('@/pages/ShareHotel'),loading: Loading});

// 我的设置
const UserSet = Loadable({loader: () => import("@/pages/UserSet"),loading: Loading});
// 我的设置
const Canvas = Loadable({loader: () => import("@/pages/Canvas"),loading: Loading});


export const history = createBrowserHistory();
export const routes = [
  {
    path:'/index',
    layout: MainLayout,
    component: Index
  },
  {
    path:'/record',
    layout: MainLayout,
    component: Record
  },
  {
    path:'/user',
    layout: MainLayout,
    component: User
  },
  {
    path:'/hotel',
    component: Hotel
  },
  {
    path:'/rankirngList',
    component: RankingList
  },
  {
    path:'/brokerage',
    component: Brokerage
  },
  {
    path:'/withdraw',
    component: Withdraw
  },
  {
    path:'/commonIssue',
    component: CommonIssue
  },
  {
    path:'/issueOne',
    component: IssueOne
  },
  {
    path:'/sharegzh',
    component: ShareGZH
  },
  {
    path:'/sharegold',
    component: ShareGold
  },
  {
    path:'/sharewx',
    component: ShareWX
  },
  {
    path:'/sharehotel',
    component: ShareHotel
  },
  {
    path:'/userSet',
    component: UserSet
  },
  {
    path:'/canvas',
    component: Canvas
  }
]


/**
 * 客房服务
 */

const ServiceIndex = Loadable({loader: () => import("@/pages/ServiceIndex"),loading: Loading});
const ServiceOrder = Loadable({loader: () => import("@/pages/ServiceOrder"),loading: Loading});
const ServiceUser = Loadable({loader: () => import("@/pages/ServiceUser"),loading: Loading});
const ServiceOrderDetail = Loadable({loader: () => import("@/pages/ServiceOrderDetail"),loading: Loading});

export const routes2 = [
  {
    path:'/sindex',
    layout: MainLayout,
    component: ServiceIndex
  },
  {
    path:'/sorder',
    layout: MainLayout,
    component: ServiceOrder
  },
  {
    path:'/suser',
    layout: MainLayout,
    component: ServiceUser
  },
  {
    path:'/sorderdetail',
    component: ServiceOrderDetail
  }
]