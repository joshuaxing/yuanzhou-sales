/*
 * @Author: your name
 * @Date: 2020-03-03 11:18:22
 * @LastEditTime: 2020-12-30 15:28:28
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\App.js
 */
import React, { Component } from "react";
import { Router } from "react-router-dom";
import { Switch, Route, Redirect } from "react-router";
import { history, routes, routes2 } from "@/router";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "@/store/actions";
import Loadable from "react-loadable";
import Loading from "@/components/Loading";

// 分销登录
const LoginPage = Loadable({
  loader: () => import("@/pages/Login/index"),
  loading: Loading,
});
// 分销忘记密码
const Forget = Loadable({
  loader: () => import("@/pages/Forget/index"),
  loading: Loading,
});
// 分销申请
const ApplyActivity = Loadable({
  loader: () => import("@/pages/ApplyActivity/index"),
  loading: Loading,
});

// const CanvasPage = Loadable({
//   loader: () => import("@/pages/Canvas/index"),
//   loading: Loading,
// });
// 首页
const Home = Loadable({
  loader: () => import("@/pages/Home/index"),
  loading: Loading,
});
// 服务登录
const ServiceLogin = Loadable({
  loader: () => import("@/pages/ServiceLogin/index"),
  loading: Loading,
});

// 页面404
const NotFound = Loadable({
  loader: () => import("@/pages/NotFound/index"),
  loading: Loading,
});

// 分销router
function getRouterByRoutes(routes, userId, currentpage) {
  let renderedRoutesList = [];
  // 登录
  const renderRoutes = (routes, parentPath) => {
    Array.isArray(routes) &&
      routes.forEach((route) => {
        const { path, redirect, children, layout, component } = route;
        if (redirect) {
          renderedRoutesList.push(
            <Redirect
              key={`${parentPath}${path}`}
              exact
              from={path}
              to={`${parentPath}${redirect}`}
            />
          );
        }
        if (component) {
          renderedRoutesList.push(
            layout ? (
              <Route
                key={`${parentPath}${path}`}
                exact
                path={`${parentPath}${path}`}
                render={(props) =>
                  React.createElement(
                    layout,
                    props,
                    React.createElement(component, props)
                  )
                }
              />
            ) : (
              <Route
                key={`${parentPath}${path}`}
                exact
                path={`${parentPath}${path}`}
                component={component}
              />
            )
          );
        }
        if (Array.isArray(children) && children.length > 0) {
          renderRoutes(children, path);
        }
      });
  };
  //未登录
  function renderLogin() {
    renderedRoutesList.push(
      <Redirect
        key="/login"
        exact
        to={{
          pathname: "/login",
        }}
      />
    );
  }

    //未登录
  function renderLogin2() {
    renderedRoutesList.push(
      <Redirect
        key="/slogin"
        exact
        to={{
          pathname: "/slogin",
        }}
      />
    );
  }

  //判断是否登陆
  console.log('userId', userId)
  console.log('currentPage', currentpage)
  if (userId) {
    renderRoutes(routes, "");
  } else {
    if (currentpage === 'index') {
      renderLogin();
    } else {
      // 客房服务
      renderLogin2();
    }
    // renderRoutes(routes, "");
  }
  return renderedRoutesList;
}

// 客房服务router
function getRouterByRoutes2(routes, userId) {
  let renderedRoutesList = [];
  // 登录
  const renderRoutes2 = (routes, parentPath) => {
    Array.isArray(routes) &&
      routes.forEach((route) => {
        const { path, redirect, children, layout, component } = route;
        if (redirect) {
          renderedRoutesList.push(
            <Redirect
              key={`${parentPath}${path}`}
              exact
              from={path}
              to={`${parentPath}${redirect}`}
            />
          );
        }
        if (component) {
          renderedRoutesList.push(
            layout ? (
              <Route
                key={`${parentPath}${path}`}
                exact
                path={`${parentPath}${path}`}
                render={(props) =>
                  React.createElement(
                    layout,
                    props,
                    React.createElement(component, props)
                  )
                }
              />
            ) : (
              <Route
                key={`${parentPath}${path}`}
                exact
                path={`${parentPath}${path}`}
                component={component}
              />
            )
          );
        }
        if (Array.isArray(children) && children.length > 0) {
          renderRoutes(children, path);
        }
      });
  };

  //判断是否登陆
  // localStorage.getItem("_yzsaleh5service"
  // console.log('login2userId', userId)
  if (userId) {
    renderRoutes2(routes, "");
  } else {
    renderLogin2();
    // renderRoutes2(routes, "");
  }
  
  // console.log('login2', renderedRoutesList)
  return renderedRoutesList;
}

@connect(
  (state) => state,
  (dispatch) => bindActionCreators(actions, dispatch)
)
class App extends Component {


  // static getDerivedStateFromProps(nextProps, prevState) {
  //   console.log(getDerivedStateFromProps)
  //   console.log(nextProps)
  //   console.log(prevState)
  // }

  componentDidMount() {

    console.log('componentDidMount')
    // 分销
    if (localStorage.getItem("_yzsaleh5user") && localStorage.getItem('page')) {
      const value = JSON.parse(localStorage.getItem("_yzsaleh5user"));
      this.props.Login(value);
      //console.log('_yzsaleh5user')
      const page = localStorage.getItem('page')
      if (page === 'index') {
        this.props.ClickPage(page);
      }
    }
    // 客房服务
    if (localStorage.getItem("_yzsaleh5service") && localStorage.getItem('page')) {
      const value2 = JSON.parse(localStorage.getItem("_yzsaleh5service"));
      this.props.Login2(value2);
      // console.log('_yzsaleh5service')
      const page2 = localStorage.getItem('page')
      if (page2 === 'sindex') {
        this.props.ClickPage(page2);
      }
    }
  }


  render() {

    const page  = this.props.other.page;
    console.log('renderpage', page)
    // console.log('renderuserId', this.props.reducer.user.userId)
    
    return (
      <Router history={history}>
        <Switch>
          <Redirect exact from="/" to="/home" />
          <Route path="/home" component={Home} />
          <Route path="/forget" component={Forget} />
          <Route path="/login" component={LoginPage} />
          <Route path="/apply" component={ApplyActivity} />
          <Route path="/slogin" component={ServiceLogin} />
          {page === 'index' && getRouterByRoutes(routes, this.props.reducer.user.userId, page)}
          {page === 'sindex' && getRouterByRoutes(routes2, this.props.service.user.userId, page)}
          <Route path="*" component={NotFound} />
        </Switch>
      </Router>
    );
  }
}
export default App;
