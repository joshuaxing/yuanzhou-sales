/*
 * @Author: your name
 * @Date: 2020-12-08 09:53:43
 * @LastEditTime: 2020-12-30 14:47:49
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-sales\src\pages\Home\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
import entry from "./entry.png";

@connect((state) => state, (dispatch) => bindActionCreators(actions, dispatch))
class Home extends React.Component {
  state = {};
  componentDidMount() {
    localStorage.removeItem("page")
  }

  handleCard(value) {
    switch (value) {
      case 1:
        if (localStorage.getItem("_yzsaleh5user")) {
          const value = JSON.parse(localStorage.getItem("_yzsaleh5user"));
          this.props.Login(value);
          this.props.ClickPage('index')
          localStorage.setItem("page", "index");
          this.props.history.push("/index");
        } else {
          this.props.ClickPage('index')
          localStorage.setItem("page", "index");
          this.props.history.replace("/login");
        }

        break;
      case 2:
        if (localStorage.getItem("_yzsaleh5service")) {
          const value = JSON.parse(localStorage.getItem("_yzsaleh5service"));
          this.props.Login2(value);
          this.props.ClickPage('sindex')
          localStorage.setItem("page", "sindex");
          this.props.history.push("/sindex");
        } else {
          this.props.ClickPage('sindex')
          localStorage.setItem("page", "sindex");
          this.props.history.replace("/slogin");
        }
       
        break;
    }
  }

  render() {
    return (
      <div className="home-page page">
        <Helmet>
          <title>远洲内部平台</title>
        </Helmet>
        <div className="card-li" onClick={this.handleCard.bind(this, 1)}>
          <img src={entry} className="card-li-image" />
          <span className="card-li-text">员工分销</span>
        </div>
        <div className="card-li" onClick={this.handleCard.bind(this, 2)}>
          <img src={entry} className="card-li-image" />
          <span className="card-li-text">客房服务</span>
        </div>
      </div>
    );
  }
}
export default Home;
