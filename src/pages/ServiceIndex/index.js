/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-12-30 13:39:06
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
import { Toast } from "antd-mobile";
import http from "@/fetch/ajax";
import Swiper from "swiper";
import "swiper/css/swiper.css"
import { STATICURL } from '@/constants/index'
import BScroll from "better-scroll";
@connect(
  state => state.service,
  dispatch => bindActionCreators(actions, dispatch)
)
class Index extends React.Component {
  constructor(props) {
    super(props);
    this.pagewrapper = React.createRef();
  }
  state = {
    bannerdata: [
      {
        id: 1,
        path: `${STATICURL}/hotel_001.png`
      },
      {
        id: 2,
        path: `${STATICURL}/hotel_002.png`
      },
      {
        id: 3,
        path: `${STATICURL}/hotel_003.png`
      }
    ],
    result: {}
  }

  componentDidMount() {
    new Swiper('.swiper-container', {
      pagination: {
        el: '.swiper-pagination',
      },
      loop: true,
      autoplay: {
        delay: 5000,
        disableOnInteraction: false
      }
    })
    this.gotData();
  }

  gotData() {
    const user = this.props.user;
    const param = {
      staffid: user.userId
    };
    http("/room/service/staff/info", {
      data: param
    }).then(result => {
      if (result.code === 0) {
        const data = result.data;
        this.setState({
          result: data
        })
      } else {
        Toast.info(result.msg);
      }
    });
  }

  gotNowDate() {
    const date = new Date();
    const nowyear = date.getFullYear();
    const nowmonth = date.getMonth() + 1;
    const nowday = date.getDate();
    const day = date.getDay();
    const weeks = ['日', '一', '二', '三', '四', '五', '六']
    const value = `${nowyear}年${this.fillZero(nowmonth)}月${this.fillZero(nowday)}日  星期${weeks[day]}`
    return value
  }

  fillZero(num) {
    return num < 10 ? "0" + num : num;
  }

  handleClick(value) {
    switch (value) {
      case 1:
        this.props.history.push("/sorder");
        break;
      default:
        Toast.info('目前没有开放，敬请期待');
    }
  }

  render() {
    const { bannerdata, result } = this.state;
    return (
      <div className="index-page page">
        <Helmet>
            <title>远洲客房服务</title>
          </Helmet>
        <div>
          <div className="index-banner">
            <div className="swiper-container" ref="swipercontainer">
              <div className="swiper-wrapper">
                {
                  bannerdata.map((item) => (
                    <div className="swiper-slide" key={item.id}>
                      <img src={item.path} className="swiper-slide-image" />
                    </div>
                  ))
                }
              </div>
              <div className="swiper-pagination"></div>
            </div>
          </div>
          <div className="marquee">
            <span className="noc-icon"></span>
            <div className="marquee-content">
              <marquee loop="infinite">2021年1月客房服务上线啦！2021年1月客房服务上线啦！2021年1月客房服务上线啦！</marquee>
            </div>
            {/* <span className="close-icon"></span> */}
          </div>
          <div className="header">
            <div className="header-h1">您好，{result.userName}</div>
            <div className="header-h2">{this.gotNowDate()}</div>
          </div>
          <div className="content">
            <ul className="content-ul">
              <li className="content-li" onClick={this.handleClick.bind(this, 1)}>
                待服务
              {
                  result.order_count ? (<span className="li-count">{result.order_count}</span>) : null
                }
              </li>
              <li className="content-li" onClick={this.handleClick.bind(this, 2)}>
                抢单
              {/* <span className="li-count"></span> */}
              </li>
              <li className="content-li" onClick={this.handleClick.bind(this, 3)}>
                排班
              {/* <span className="li-count"></span> */}
              </li>
              <li className="content-li" onClick={this.handleClick.bind(this, 4)}>
                统计
              {/* <span className="li-count"></span> */}
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}
export default Index;
