/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-12-28 13:55:00
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
import { Toast } from "antd-mobile";
import http from "@/fetch";
import Navigationbar from "@/components/Navigationbar/index";
import Swiper from "swiper";
import "swiper/css/swiper.css";
import hotelgb from "./hotelgb.jpg";
import { STATICURL } from "@/constants/index";
// let yzbasepath = 'https://api.snhotelsgroup.com/image/common';
@connect(
  (state) => state.reducer,
  (dispatch) => bindActionCreators(actions, dispatch)
)
class Index extends React.Component {
  constructor(props) {
    super(props);
    this.pagewrapper = React.createRef();
  }
  state = {
    bannerdata: [
      {
        id: 1,
        path: `${STATICURL}/hotel_001.png`,
      },
      {
        id: 2,
        path: `${STATICURL}/hotel_002.png`,
      },
      {
        id: 3,
        path: `${STATICURL}/hotel_003.png`,
      },
    ],
    rootList: [],
  };

  componentDidMount() {
    new Swiper(".swiper-container", {
      pagination: {
        el: ".swiper-pagination",
      },
      loop: true,
      autoplay: {
        delay: 5000,
        disableOnInteraction: false,
      },
    });
    this.handleCheck();
  }
  handleCheck() {
    const user = this.props.user;
    const param = {
      saleid: user.userId
    };
    http("/sale/user/activity", {
      data: param,
    }).then((result) => {
      if (result.code === 0) {
        const data = result.data;
        this.setState({
          rootList: data.activitytype,
        });
      } else {
        Toast.info(result.msg);
      }
    });
  }
  handleNav(value) {
    const { rootList } = this.state;
    const isroot = rootList.some((rootvalue) => {
      return rootvalue === value;
    });
    if (!isroot) {
      Toast.fail("您目前没有该权限");
      return;
    }
    switch (value) {
      case 1:
        // 公众号
        this.props.history.push("/sharegzh");
        break;
      case 2:
        //分销注册会员
        this.props.history.push("/sharewx");
        break;
      case 3:
        //分销金卡
        this.props.history.push("/sharegold");
        break;
      case 4:
        //订房分销
        this.props.history.push("/hotel");
        break;
      default:
    }
  }

  render() {
    const { bannerdata } = this.state;
    return (
      <div className="index-page page" ref={this.pagewrapper}>
        <Helmet>
          <title>远洲酒店分销</title>
        </Helmet>
        {/* <Navigationbar title="员工分销平台"/> */}
        <div>
          <div className="index-banner">
            <div className="swiper-container" ref="swipercontainer">
              <div className="swiper-wrapper">
                {bannerdata.map((item) => (
                  <div className="swiper-slide" key={item.id}>
                    <img src={item.path} className="swiper-slide-image" />
                  </div>
                ))}
              </div>
              <div className="swiper-pagination"></div>
            </div>
          </div>
          <div className="toolbar">
            <ul className="toolbar-ul">
              <li className="toolbar-li" onClick={this.handleNav.bind(this, 1)}>
                <span className="toolbar-li-icon gzh-icon"></span>
                <span className="toolbar-li-text">关注公众号</span>
              </li>
              <li className="toolbar-li" onClick={this.handleNav.bind(this, 3)}>
                <span className="toolbar-li-icon jk-icon"></span>
                <span className="toolbar-li-text">金卡会籍</span>
              </li>
              <li className="toolbar-li" onClick={this.handleNav.bind(this, 2)}>
                <span className="toolbar-li-icon hy-icon"></span>
                <span className="toolbar-li-text">会员注册</span>
              </li>
              <li className="toolbar-li" onClick={this.handleNav.bind(this, 4)}>
                <span className="toolbar-li-icon jd-icon"></span>
                <span className="toolbar-li-text">酒店预定</span>
              </li>
            </ul>
          </div>
          <div className="marquee">
            <span className="noc-icon"></span>
            <div className="marquee-content">
              <marquee loop="infinite">
                2020年6月分销平台上线啦！2020年6月分销平台上线啦！2020年6月分销平台上线啦！
            </marquee>
            </div>
            {/* <span className="close-icon"></span> */}
          </div>
          <div className="activity-box border-1px">
            <div className="activity-title">人气活动</div>
            <ul className="activity-ul">
              <li className="activity-li">
                <img src={hotelgb} className="li-image" />
                <span className="li-mask">敬请期待</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}
export default Index;
