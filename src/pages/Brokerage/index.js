/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-06-18 10:49:37
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
import http from "@/fetch";
import { Toast } from "antd-mobile";
import BackBar from "@/components/BackBar/index";
import NotData from "@/components/NotData/index";
@connect(
  (state) => state.reducer,
  (dispatch) => bindActionCreators(actions, dispatch)
)
class Brokerage extends React.Component {
  state = {
    result: {},
    recordList: [],
    nodata: false,
  };

  componentDidMount() {
    this.gotData();
  }
  goWithdraw() {
    this.props.history.push("/withdraw");
  }
  gotData() {
    const user = this.props.user;
    const param = {
      saleid: user.userId,
    };
    http("/sale/user/commission", {
      data: param,
    }).then((result) => {
      if (result.code === 0) {
        const data = result.data ? result.data : {};
        const recordList = data.recordList ? data.recordList : [];
        const nodata = recordList.length > 0 ? false : true;
        const date = new Date();
        const year = date.getFullYear();
        const month = date.getMonth() + 1;
        const now = year + "-" + this.fillZero(month);
        recordList.map((item) => {
          if (now === item.date) {
            item.isnow = true;
          }
          item.datestr = item.date ? item.date.replace(/-/g, '年') + '月' : ''
        });
        this.setState({
          result: data,
          recordList: recordList,
          nodata: nodata,
        });
      } else {
        Toast.info(result.msg);
      }
    });
  }
  handlePirce(value) {
    return typeof value === "number" ? value.toFixed(2) : "0.00";
  }
  fillZero(value) {
    return value < 10 ? "0" + value : value;
  }
  render() {
    const { result, recordList, nodata } = this.state;
    return (
      <div className="brokerage-page page">
        <Helmet>
          <title>我的佣金</title>
        </Helmet>
        <BackBar></BackBar>
        <div className="brokerage-header">
          <div className="header-content">
            <div className="brokerage-text">预估本月可提现佣金</div>
            <div className="brokerage-price">
              ¥{this.handlePirce(result.amount)}
            </div>
            <div className="brokerage-des">
              每月财务结算好之后，可提现上月的佣金备份
            </div>
          </div>
          <div className="header-qianbao" onClick={this.goWithdraw.bind(this)}>
            <div className="qianbao-box">
              <span className="qianbao-icon"></span>
              <span className="qianbao-text">提现</span>
            </div>
          </div>
          <div className="card-box">
            <span className="card-title">累计已提现佣金</span>
            <span className="card-price">
              ¥{this.handlePirce(result.withdraw_amount)}
            </span>
          </div>
        </div>
        <div className="brokerage-content">
          {recordList.map((item, index) => (
            <ul className="brokerage-ul" key={String(index)}>
              <li className="brokerage-li-header">
                <span className="brokerage-item">
                  {item.isnow ? `本月预估收入` : `${item.datestr}已核算收入`}
                </span>
                <span className="brokerage-item">
                  ¥{this.handlePirce(item.monthIncome.total)}
                </span>
              </li>
              <li className="brokerage-li">
                <span className="brokerage-item">预订/活动收入：</span>
                <span className="brokerage-item">
                  ¥{this.handlePirce(item.monthIncome.activity)}
                </span>
              </li>
              <li className="brokerage-li">
                <span className="brokerage-item">注册会员收入：</span>
                <span className="brokerage-item">
                  ¥{this.handlePirce(item.monthIncome.member_register)}
                </span>
              </li>
              <li className="brokerage-li">
                <span className="brokerage-item">金卡分享收入：</span>
                <span className="brokerage-item">
                  ¥{this.handlePirce(item.monthIncome.goldcard_sale)}
                </span>
              </li>
              <li className="brokerage-li">
                <span className="brokerage-item">关注公众号人数：</span>
                <span className="brokerage-item">
                  {item.monthIncome.attention_count
                    ? item.monthIncome.attention_count
                    : 0}
                  人
                </span>
              </li>
            </ul>
          ))}
          {nodata && <NotData></NotData>}
        </div>
      </div>
    );
  }
}
export default Brokerage;
