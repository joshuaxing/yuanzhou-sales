/*
 * @Author: your name
 * @Date: 2020-03-03 11:46:41
 * @LastEditTime: 2020-12-08 14:51:19
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\layouts\MainLayout\index.js
 */
import React from "react";
import "./style.scss";
import Nav from "@/components/Nav";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as actions from "@/store/actions";

@connect(
  (state) => state,
  (dispatch) => bindActionCreators(actions, dispatch)
)
class MainLayout extends React.Component {
  state = {
    navList: [
      {
        name: '首页',
        icon: 'home',
        path: '/index'
      },
      {
        name: '佣金记录',
        icon: 'record',
        path: '/record'
      },
      {
        name: '我的',
        icon: 'user',
        path: '/user'
      }
    ],
    navList2: [
      {
        name: '首页',
        icon: 'home',
        path: '/sindex'
      },
      {
        name: '订单',
        icon: 'order',
        path: '/sorder'
      },
      {
        name: '我的',
        icon: 'user',
        path: '/suser'
      }
    ],
    data: []
  }

  componentDidMount() {
    // console.log('layouts')
    const page  = this.props.other.page;
    const data = page === 'index' ? this.state.navList : this.state.navList2;
    this.setState({
      data: data
    })
  }

  render() {
    const {children} = this.props;
    return <div className="page app-page">
        <div 
          className="app-page-body" 
          ref={this.pagewrapper}
        >
          {children}
        </div>
        <Nav navdata={this.state.data}/>
    </div>;
  }
}
export default MainLayout;
