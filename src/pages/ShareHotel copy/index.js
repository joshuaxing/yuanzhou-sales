/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-10-22 17:44:52
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
import http from "@/fetch";
import { Toast } from "antd-mobile";
import Loading from "@/components/Loading";
import BackBar from "@/components/BackBar";
import { RESOURCEURL, QLOGO} from '@/constants';
import hotelgb from './hotelgb.jpg'

@connect(
  state => state.reducer,
  dispatch => bindActionCreators(actions, dispatch)
)
class ShareHotel extends React.Component {
  isdraw = false
  state = {
    showimage: false,
    codeurl: "",
    headurl: "",
    nickname: "",
    isload: true,
    result: {},
    line: [],
    imagelist: [],
    loadImages: []
  };
  componentDidMount() {
    const id = this.props.location.state.id
    let line = new Array(50).fill(0)
    this.setState({
      line: line
    })
    this.gotData(id);
  }
  gotData(id) {
    const user = this.props.user;
    const param = {
      saleid: user.userId,
      hotelbookingid: id
    };
    http("/sale/user/roomdetail", {
      data: param
    }).then(result => {
      if (result.code === 0) {
        const data = result.data;
        document.getElementById('nickname').innerHTML =  data.nickname ?   data.nickname : '';
        document.getElementById('hotelname').innerHTML = data.hotelname ?  data.hotelname : '';
        document.getElementById('hotelroom').innerHTML = data.roomname ?  data.roomname : '';
        document.getElementById('hotelprice').innerHTML = data.roomprice ?  data.roomprice : '';
        document.getElementById('hoteltips').innerHTML = data.title ? '“' + data.title + '”' : '';
        // 图片汇总统一加载图片
        let loadImages = [];
        const originurl = window.location.protocol+ "//" + window.location.host + '/image';
        let originimagelist = data.imagelist ? data.imagelist : [];
        let imagelist = []
        originimagelist.map((item) => {
          imagelist.push(item.replace(RESOURCEURL, originurl))
        })
        const code_url = data.code_url;
        let codeurl = code_url;
        if (code_url) {
          codeurl = code_url.replace(RESOURCEURL, originurl)
        }
        const logourl = data.logourl;
        let headurl = logourl;
        const originlogourl = window.location.protocol + "//" + window.location.host + '/logo';
        if (logourl) {
          headurl = logourl.replace(QLOGO, originlogourl)
        }
        loadImages.push(codeurl)
        loadImages.push(headurl)
        loadImages = [...imagelist,...loadImages]
        console.log(loadImages)
        // console.log(codeurl)
        // console.log(headurl)
        this.setState({
          codeurl: codeurl,
          headurl: headurl,
          imagelist: imagelist,
          result: data,
          loadImages: loadImages
        });
        this.getImagesInfo();
      } else {
        Toast.info(result.msg);
      }
    });
  }

  loadCompleted() {
    window.pageYOffset = 0;
    document.documentElement.scrollTop = 0;
    document.body.scrollTop = 0;
    setTimeout(() => {
      this.initCanvas();
    }, 1000)
  }
  // 获取图片信息
  getImagesInfo () {
    let imageList = [];
    const views = this.state.loadImages;
    for (let i = 0; i < views.length; i++) {
      imageList.push(this.getImageInfo(views[i]))
    }
    Promise.all(imageList).then((result) => {
      window.pageYOffset = 0;
      document.documentElement.scrollTop = 0;
      document.body.scrollTop = 0;
      console.log('全部图片加载完成')
      setTimeout(() => {
        this.initCanvas();
      }, 1000)
    }).catch((error) => {
      Toast.info('图片加载失败，请刷新重试');
    })
  }
  // 返回图片对象
  getImageInfo (path) {
    return new Promise((resolve, reject) => {
      let img = new Image();
      // img.setAttribute("crossOrigin", "Anonymous");
      img.onload = function () {
        resolve(img)
      }
      img.onerror = function () {
        reject('图片加载失败')
      }
      img.src = path;
    })
  }

  initCanvas() {
    const that = this;
    const targetDom = document.getElementById("capture");
    html2canvas(targetDom, {
      allowTaint: false,
      useCORS: true,
      height: targetDom.clientHeight,
      width: targetDom.clientWidth
    }).then(canvas => {
      var url = canvas.toDataURL();
      var image = new Image();
      image.src = url;
      image.style.width = `${targetDom.clientWidth}px`;
      image.style.height = `${targetDom.clientHeight}px`;
      image.onload = function() {
        that.setState(
          {
            showimage: true,
            isload: false
          },
          () => {
            image.style.transform = `scale(0.9)`;
          }
        );
      };
      document.getElementById("image-box").appendChild(image);
    });
  }
  render() {
    const { showimage, codeurl, headurl, isload, result, line, imagelist} = this.state;
    return (
      <div className="sharehotel-page page">
        <Helmet>
          <title>酒店预定分享</title>
        </Helmet>
        <BackBar></BackBar>
        {!showimage ? (
          <div className="sharehotel-content-wrapper">
            <div className="sharehotel-content" id="capture">
              <div className="hotel-name" id="hotelname"></div>
              <div className="hotel-tips" id="hoteltips"></div>
              <div className="hotel-room" id="hotelroom"></div>
              <div className="hotel-pirce"><span className="pirce-symbol">￥</span><span className="pirce" id="hotelprice"></span><span className="price-text">起</span></div>
              <div className="hotel-images">
                {
                  imagelist.map((item, index) => (
                    <div className="hotel-image-item" key={String(index)}>
                      <img src={item} className="hotel-image"/>
                    </div>
                  ))
                }
              </div>
              <div className="hotel-lines">
                {
                  line.map((item, index) => (
                  <div className="hotel-line" key={String(index)}></div>
                  ))
                }
              </div>
              <div className="code-wrapper">
                <div className="hotelcode-box">
                  <img src={codeurl} className="hotelcode"/>
                </div>
                <div className="user-box">
                  <div className="user-content">
                    <img
                      src={headurl}
                      className="user-image"
                      alt="加载失败，重新加载"
                    />
                    <span className="user-text">来自<span id="nickname" className="nickname"></span>的分享</span>
                  </div>
                  <div className="user-tips user-tips-bottom">长按识别小程序</div>
                  <div className="user-tips">立即预定酒店</div>
                </div>
              </div>
            </div>
          </div>
        ) : null}

        {isload && (
          <div className="load-mask">
            <Loading></Loading>
          </div>
        )}

        <div className="create-logo">
          <div id="image-box"></div>
          <div className={`create-logo-tips`} id="image-text">
            <div className={`tips-p ${!isload ? "active" : ""}`}>
              长按可保存图片，分享至微信群或朋友圈。
            </div>
            <div className={`tips-p ${!isload ? "active" : ""}`}>
              好友入住成功后您将获得
              <span className="tips-p-span">{result.commissiontype === 2 ? result.commission_amount + '元' : "订单金额" + (result.commission_amount*100).toFixed(2)+'%' }</span>佣金奖励噢~
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default ShareHotel;
