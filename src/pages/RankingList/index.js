/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-05-29 14:23:47
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
import http from "@/fetch";
import { Toast } from "antd-mobile";
import headicon from './head-icon@2x.png'
import NotData from "@/components/NotData";
import BackBar from "@/components/BackBar/index";
@connect(
  state => state.reducer,
  dispatch => bindActionCreators(actions, dispatch)
)
class RankingList extends React.Component {

  state = {
    navindex: 1,
    result: {
      monthly_ranking: '',
      nickname: '',
      day_ranking: '',
      ranking: '',
      logourl: '',
      ranking_list: []
    },
    notdata: false
  }

  componentDidMount() {
    this.gotData();
  }

  clickNav (value) {
    this.setState({
      navindex: value
    }, () => {
      this.gotData()
    })
  }
  gotData () {
    const user = this.props.user
    const param = {
      saleid: user.userId,
      rangkingtype: this.state.navindex
    }
    http("/sale/user/ranking", {
      data: param
    }).then(result => {
      if (result.code === 0) {
        const data = result.data;
        const notdata = data.ranking_list ? data.ranking_list.length > 0 ? false : true : true;
        this.setState({
          result: data,
          notdata: notdata
        })
      } else {
        Toast.info(result.msg);
      }
    });
  }
  render() {
    const {navindex, result, notdata} = this.state;
    return (
      <div className="ranking-page page">
        <Helmet>
          <title>排行榜</title>
        </Helmet>
        <BackBar></BackBar>
        <div className="ranking-header">
          <div className="header-content">
            <img src={result.logourl ? result.logourl : headicon} className="header-image"/>
            <div className="header-other">
              <span className="header-name">{result.nickname}</span>
            </div>
          </div>
          <div className="month-statistics">
            <div className="statistics-rank">{result.monthly_ranking ? `NO.${result.monthly_ranking}` : '暂无'}</div>
            <div className="statistics-time">本月佣金排名</div>
          </div>
        </div>
        <ul className="ranking-navs">
          <li className={`ranking-nav ${ navindex === 1 ? 'active' : ''}`} onClick={this.clickNav.bind(this, 1)}>佣金排名</li>
          <li className={`ranking-nav ${ navindex === 2 ? 'active' : ''}`} onClick={this.clickNav.bind(this, 2)}>推荐排名</li>
        </ul>
        <div className="ranking-statistics-box">
          <div className="ranking-statistics">
            <div className="statistics-title">{navindex === 1 ? '佣金' : '推荐'}排名</div>
            <ul className="statistics-ul">
              <li className="statistics-li">
                <div className="statistics-li-title">日排名</div>
                <div className="statistics-li-num">{result.day_ranking ? result.day_ranking : '暂无'}</div>
              </li>
              <li className="statistics-li">
                <div className="statistics-li-title">月排名</div>
                <div className="statistics-li-num num-m">{result.monthly_ranking ? result.monthly_ranking : '暂无'}</div>
              </li>
              <li className="statistics-li">
                <div className="statistics-li-title">总排名</div>
                <div className="statistics-li-num num-t">{result.ranking ? result.ranking : '暂无'}</div>
              </li>
            </ul>
          </div>
        </div>
        <div className="ranking-list-box">
          <div className="ranking-list">
            <div className="ranking-list-title">{navindex === 1 ? '佣金' : '推荐'}日排名Top10</div>
            <ul className="ranking-list-ul">
              {
                result.ranking_list && result.ranking_list.map((item, index) => (
                  <li className="ranking-list-li" key={String(index)}>
                    <div className="list-li-image">
                      <img src={item.logourl ? item.logourl : headicon} className="li-image" />
                    </div>
                    <div className="list-li-content">
                      <span className="name">{item.truename}</span>
                      <span className="job">{item.department}</span>
                      {
                        navindex === 1 ? (<span className="des">今日收益:¥{item.income}</span>) : (<span className="des">推荐人数:{item.count}人</span>)
                      }
                    </div>
                    <div className="list-li-num">NO.{item.ranking}</div>
                  </li>
                ))
              }
            </ul>
            {
              notdata &&  <NotData></NotData>
            }
          </div>
        </div>
      </div>
    );
  }
}
export default RankingList;
