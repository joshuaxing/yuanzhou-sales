/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-05-20 17:53:48
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
import BScroll from "better-scroll";

@connect(
  state => state,
  dispatch => bindActionCreators(actions, dispatch)
)
class Hotel extends React.Component {
  state = {
    navindex: 1
  };

  componentDidMount() {
    let scroll = new BScroll(this.refs.hotelwrapper, {});
  }

  clickNav(value) {
    this.setState({
      navindex: value
    });
  }
  computedHeight(vheight) {
    const pageheight = document.body.clientHeight;
    return pageheight - vheight + "px";
  }
  render() {
    const { navindex } = this.state;
    return (
      <div className="hotel-page page">
        <Helmet>
          <title>订房分享</title>
        </Helmet>
        <div className="type-navs">
          <div
            className={`type-nav ${navindex === 1 ? "active" : ""}`}
            onClick={this.clickNav.bind(this, 1)}
          >
            活动中
          </div>
          <div
            className={`type-nav ${navindex === 2 ? "active" : ""}`}
            onClick={this.clickNav.bind(this, 2)}
          >
            已过期
          </div>
        </div>
        <div
          className="hotel-ul-wrapper"
          ref="hotelwrapper"
          style={{ height: this.computedHeight(44) }}
        >
          <ul className="hotel-ul">
            <li className="hotel-li">
              <div className="hotel-li-image">
                {/* <img src={headicon} className="li-image" /> */}
              </div>
              <div className="hotel-li-content">
                <span className="hotel-name">杭州庐境西溪酒店</span>
                <span className="hotel-room">豪华商务套房</span>
                <span className="hotel-profit">预计佣金：订单金额 2%</span>
              </div>
              <div className="store-btn">我要分享</div>
            </li>
            <li className="hotel-li">
              <div className="hotel-li-image">
                {/* <img src={headicon} className="li-image" /> */}
              </div>
              <div className="hotel-li-content">
                <span className="hotel-name">杭州庐境西溪酒店</span>
                <span className="hotel-room">豪华商务套房</span>
                <span className="hotel-profit">预计佣金：订单金额 2%</span>
              </div>
              <div className="store-btn">我要分享</div>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
export default Hotel;
