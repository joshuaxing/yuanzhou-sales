/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-06-11 16:06:18
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
@connect(
  (state) => state.reducer,
  (dispatch) => bindActionCreators(actions, dispatch)
)
class CanvasPage extends React.Component {
  static defaultProps = {
    canvaswidth: 750, // 画布宽度
    canvasheight: 1334, // 画布高度
  };
  state = {
    per: 1,
  };

  constructor(props) {
    super(props);
    this.canvas = React.createRef();
  }

  componentDidMount() {
    const per = this.gotper();
    this.setState(
      {
        per: per,
      },
      () => {
        this.initCanvas();
      }
    );
  }

  componentWillUnmount() {}

  initCanvas() {
    const canvas = this.canvas.current;
    if (canvas.getContext) {
      var ctx = canvas.getContext("2d");
      (function () {
        Object.getPrototypeOf(ctx).Triangle = function (x, y, r) {
          this.save();
          this.translate(x, y);
          this.rotate(r);
          this.beginPath();
          this.moveTo(0, 0);
          this.lineTo(10, 0);
          this.lineTo(0, 10);
          this.lineTo(-10, 0);
          this.closePath();
          this.fill();
          this.restore();
        };
        Object.getPrototypeOf(ctx).line = function (x, y, x1, y1) {
          this.save();
          this.beginPath();
          this.moveTo(x, y);
          this.lineTo(x1, y1);
          this.stroke();
          this.restore();
        };
      })();
      ctx.strokeStyle = "#7C8B8C";
      ctx.line(90, 130, 320, 210);
      ctx.Triangle(320, 210, -Math.PI * 0.4);
    }
  }

  createImg() {
    let img = new Image();
    img.setAttribute("crossOrigin", "Anonymous");
    img.src = url;
    return img;
  }
  convertCanvasToImage() {
    let image = new Image();
    image.src = canvas.toDataURL("image/png");
    return image;
  }
  dpr() {
    if (window.devicePixelRatio && window.devicePixelRatio > 1) {
      return window.devicePixelRatio;
    }
    return 1;
  }

  gotper() {
    const systemWidth =
      document.documentElement.clientWidth || document.body.clientWidth;
    const scale = (systemWidth / 750).toFixed(2);
    return scale;
  }

  render() {
    const { canvaswidth, canvasheight } = this.props;
    const { per } = this.state;
    return (
      <div className="canvas-page page">
        <Helmet>
          <title>Canvas</title>
        </Helmet>
        <canvas
          width={canvaswidth * per}
          height={canvasheight * per}
          ref={this.canvas}
        ></canvas>
      </div>
    );
  }
}
export default CanvasPage;
