/*
 * @Author: your name
 * @Date: 2020-03-21 22:55:19
 * @LastEditTime: 2020-05-13 17:58:51
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\components\NotData\index.js
 */
import React from 'react';
import './style.scss';
// import notdataicon from './nodata-icon@2x.png'

class Navigationbar extends React.Component {
    static defaultProps = {
      title: '标题'
    }
    constructor (props){
      super(props)
    }
    render () {

      const { title } = this.props;

      return (
        <div className="navigationbar">
          <div className="navigationbar-title">{title}</div>
        </div>
      )
    } 

}

export default Navigationbar