/*
 * @Author: your name
 * @Date: 2020-03-05 10:00:10
 * @LastEditTime: 2020-12-30 14:08:38
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-salesH5\src\pages\Login\index.js
 */
import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import * as actions from "@/store/actions";
import { Toast } from "antd-mobile";
import http from "@/fetch/ajax";
import headicon from "./head-icon@2x.png";
@connect(
  state => state.service,
  dispatch => bindActionCreators(actions, dispatch)
)
class Set extends React.Component {

  state = {
    result: {
      nickname: "",
      logourl: "",
      phone: '',
      truename: ''
    }
  }
  componentDidMount() {
    this.gotData();
  }

  componentWillUnmount () {}

  gotData() {
    const user = this.props.user;
    const param = {
      staffid: user.userId
    };
    http("/room/service/staff/info", {
      data: param
    }).then(result => {
      if (result.code === 0) {
        const data = result.data;
        this.setState({
          result: data
        });
      } else {
        Toast.info(result.msg);
      }
    });
  }
  handleBack () {
    this.props.history.push('/')
    localStorage.removeItem("_yzsaleh5service");
    const user = {
      userId: 0,
      token: ""
    };
    //退出登陆
    this.props.Login2(user)
  }
  render() {
    const {result} = this.state;
    return (
      <div className="set-page page">
        <Helmet>
          <title>我的</title>
        </Helmet>
        <ul className="table-ul">
          <li className="table-li table-li">
            <span className="table-li-title">头像</span>
            <span className="table-li-content">
              <img src={result.logourl ? result.logourl : headicon} className="table-li-icon"/>
            </span>
          </li>
          <li className="table-li">
            <span className="table-li-title">姓名</span>
            <span className="table-li-content">{result.userName ? result.userName : ''}</span>
          </li>
          <li className="table-li">
            <span className="table-li-title">手机号</span>
            <span className="table-li-content">{result.phone ? result.phone: ''}</span>
          </li>
        </ul>
        <div className="back-btn-box">
          <div className="back-btn" onClick={this.handleBack.bind(this)}>退出登陆</div>
        </div>
      </div>
    );
  }
}
export default Set;
