import React from "react";
import "./style.scss";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import { Toast } from "antd-mobile";
import http from "@/fetch/ajax";
import * as actions from "@/store/actions";
import loginbanner from "./loginbanner.jpg";

@connect(
  (state) => state.reducer,
  (dispatch) => bindActionCreators(actions, dispatch)
)
class ServiceLogin extends React.Component {
  codeTimer = null;
  state = {
    phone: "",
    code: "",
    lasttime: false,
    ischeck: false,
    disabledCode: true,
    codetext: "获取验证码",
    timenum: 60,
  };

  componentDidMount() {}

  handleBlur() {
    window.scrollTo(0, 0);
  }
  handlePhoneInput = (e) => {
    const value = e.target.value;
    if (!/^1\d{10}$/.test(value)) {
      this.setState({
        ischeck: false,
        phone: value,
        disabledCode: true,
      });
    } else {
      //手机号正确
      if (!this.state.lasttime) {
        this.setState({
          disabledCode: false,
          ischeck: true,
          phone: value,
        });
      } else {
        //倒计时中
        this.setState({
          disabledCode: true,
          ischeck: true,
          phone: value,
        });
      }
    }
  };

  submitCode = () => {
    const { phone, disabledCode } = this.state;
    if (disabledCode) {
      return;
    }

    const params = {
      phone: phone,
    };

    //获取验证码
    http("/room/service/staff/verify", {
      data: params,
    }).then((result) => {

      if (result.code === 0) {
        // 倒计时
        this.countTime();
      }

      Toast.info(result.msg);

    });
  };

  handleCodeInput = (e) => {
    const value = e.target.value;
    this.setState({
      code: value,
    });
  };

  countTime() {
    //倒计时
    this.setState(
      {
        codetext: this.state.timenum + "s",
        disabledCode: true,
        lasttime: true,
      },
      () => {
        this.codeTimer && clearInterval(this.codeTimer);
        this.codeTimer = setInterval(() => {
          this.setState((state, props) => ({
            timenum: state.timenum - 1,
            codetext: state.timenum + "s",
          }));
          if (this.state.timenum <= 0) {
            this.resetTime();
          }
        }, 1000);
      }
    );
  }
  resetTime() {
    //重置倒计时
    clearInterval(this.codeTimer);
    const disabledCode = this.state.ischeck ? false : true;
    this.setState({
      disabledCode: disabledCode,
      codetext: "获取验证码",
      timenum: 60,
      lasttime: false,
    });
  }
  submitLogin = () => {
    this.submitCodeLogin();
  };
  submitCodeLogin() {
    const { phone, code } = this.state;
    if (!/^1\d{10}$/.test(phone)) {
      Toast.info("手机号码不合法");
    } else if (!code) {
      Toast.info("请输入验证码");
    } else {
      const paramdata = {
        phone: phone,
        verifycode: code,
      };
      http("/room/service/staff/loginVerify", {
        data: paramdata,
      }).then((result) => {
        if (result.code === 0) {
          const data = result.data;
          const value = {
            userId: data.id,
            token: data.token
          };
          
          //判断是否登陆
          this.props.Login2(value);
          localStorage.setItem("_yzsaleh5service", JSON.stringify(value));
          this.props.ClickPage('sindex')
          setTimeout(() => {
            this.props.history.replace("/sindex");
          }, 20)
        } else {
          Toast.info(result.msg);
        }
      });
    }
  }
  clearFromValue(value) {
    switch (value) {
      case 1:
        this.setState({
          phone: "",
        });
        break;
    }
  }

  render() {
    const { phone, code, disabledCode, codetext } = this.state;
    return (
      <div className="login-page page">
        <Helmet>
          <title>远洲客房服务登录</title>
        </Helmet>
        <div className="login-banner">
          <img src={loginbanner} className="banner" />
        </div>

        <div className="form-ul">
          <div className="form-li border-1px">
            <span className="form-li-icon phone-icon"></span>
            <input
              type="number"
              className="form-li-input"
              placeholder="请输入手机号码"
              onChange={this.handlePhoneInput}
              value={phone}
              maxLength={11}
              onBlur={this.handleBlur}
            />
            {phone ? (
              <span
                className="form-li-icon close-icon"
                onClick={this.clearFromValue.bind(this, 1)}
              ></span>
            ) : null}
          </div>
          <div className="form-li border-1px">
            <span className="form-li-icon code-icon"></span>
            <input
              type="number"
              className="form-li-input li-input-width"
              placeholder="请输入验证码"
              onChange={this.handleCodeInput}
              value={code}
              maxLength={6}
              onBlur={this.handleBlur}
            />
            <span
              className={`form-li-btn ${disabledCode ? "" : "li-btn-active"}`}
              onClick={this.submitCode}
            >
              {codetext}
            </span>
          </div>
          <div className="submit-btn" onClick={this.submitLogin.bind(this)}>
            登录
          </div>
        </div>
      </div>
    );
  }
}
export default ServiceLogin;
