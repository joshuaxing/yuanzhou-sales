/*
 * @Author: your name
 * @Date: 2020-12-08 14:18:45
 * @LastEditTime: 2020-12-30 11:30:18
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \yuanzhou-sales\src\fetch\ajax.js
 */
import http from '@/utils/http'
let baseURL = '/api/sale/login';
/**
 * @param  {Objece} params 参数
 * @return {Promise}       包含抓取任务的Promise
 */
export default (api, params, other) => {
  let url = baseURL + api;
  let defaultOption = {
    isload: true
  }
  let option = {};
  if (other) {
    option = Object.assign({}, defaultOption, other);
  } else {
    option = Object.assign({}, defaultOption);
  }
  return http.post2(url, params, option)
}